<?php

use sms\Sms_lib;

/**
 * Description of SmsAdapter
 *
 * @author Tohir
 */
class SmsAdapter {

    //put your code here
    protected static $message = "Welcome to Healthcare Internationl. The best HMO provider in Nigeria";

    public static function sendSms($mobiles, $message=null) {
        if ($mobiles != '') {

//            return Sms_lib::sendSms(static::preparePhoneNumber($mobiles), static::$message);
            return Sms_lib::sendSmsHit(static::preparePhoneNumber($mobiles), is_null($message) ? static::$message : $message);
        }
    }

    private static function preparePhoneNumber($phone) {
        if (substr($phone, 0, 1) === '0') {
            $phone = '234' . substr($phone, 1);
        }

        return $phone;
    }

}

//var_dump(SmsAdapter::processViaSMSRoute('2348023611841', 'Test message'));
