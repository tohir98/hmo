<?php

namespace sms;

/**
 * Description of sms_lib
 *
 * @author chuksolloh
 */
class Sms_lib {

    private $CI;
    //put your code here
    protected static $user = 'otcleantech@gmail.com'; // Username
    protected static $pass = 'P@$$w0rd123'; // Password
    protected static $url = 'http://smsgator.com/bulksms'; // SMS Url
    
    //For SMS Hit
    protected static $api_url = 'http://api.smshit.net/index.php/push_sms/'; // SMS Url
    protected static $acctKey = 'a4f85b620e';

    public function __construct() {
        // Load CI object
        $this->CI = get_instance();
        $this->CI->load->database();
    }

    /**
     * Sends SMS to SMS gateway
     * 
     * @param type $mobiles
     * @param type $message
     * @return array | string
     */
    public static function sendSms($mobiles, $message) {
		if ($mobiles != '' && $message != '') {
			$ret = self::cUrl(self::$url .'?email='. self::$user .'&password='. self::$pass .'&type=0&dlr=0&destination='. urlencode($mobiles) .'&sender=FBNLounge&message='. urlencode($message));
			return array(
				'message' => self::errCode()[self::decrpytResponse($ret)],
				'code' => $ret != 2601 ? 0 : 1 
			);
		}
	}
        
        public static function sendSmsHit($mobile, $message) {
            //http://api.smshit.net/index.php/push_sms/:account_key/:message/:destination/:sender_id
            if ($mobile != '' && $message != '') {
			$ret = self::cUrl(self::$api_url . self::$acctKey .'/'. urlencode($message) .'/'. urlencode($mobile) .'/HealthCare');
                        $response = json_decode($ret);
			return array(
				'message' => $response->message,
				'status' => $response->status 
			);
		}
        }
	
	public static function checkBalance() {
		$code = self::cUrl(self::$url . '/balance?email='. self::$user . '&password=' . self::$pass);
		return self::errCode()[self::decrpytResponse($code)] != null ? : $code;
	}
	
	protected static function cUrl($url) { //echo $url; exit;
		if ($url != '') {
			try {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				$ret = curl_exec($ch); 
				curl_close($ch);
				
				return $ret;
			}catch(Exception $ex) {
				echo $ex->getMessage();
			}
		}
	}
	
	protected static function decrpytResponse($str){
		if ($str != '') {
			$ret = explode('|', $str);
			return $ret[0] ? : null;
		}
	}

	protected static function errCode() {
		return array(
			2601 => 'Success',
			2602 => 'Invalid Url',
			2603 => 'Invalid Username/Password',
			2604 => 'Invalid Type',
			2605 => 'Invalid Message',
			2606 => 'Invalid Destination',
			2607 => 'Invalid Sender',
			2608 => 'Invalid Dlr',
			2609 => 'Invalid Scheduled Date Format',
			2610 => 'Insufficient Credit',
			2611 => 'Message Sending/Scheduling Failed'
		);
	}

    public function send_sms($data) {

        $status = self::sendSms($this->_preparePhoneNumber($data['recepient']), $data['message']);

        return $this->CI->db->insert('sms_messages', array(
                'phone_number' => $this->_preparePhoneNumber($data['recepient']),
                'message' => $data['message'],
                'school_id' => $this->CI->user_auth_lib->get('school_id'),
                'sender_id' => $this->CI->user_auth_lib->get('user_id'),
                'module_id' => $data['module_id'],
                'date_sent' => date('Y-m-d'),
                'time_sent' => date('h:i:s'),
                'units' => 1,
                'status' => $status
        ));
    }

    private function _preparePhoneNumber($phone) {
        if (substr($phone, 0, 1) === '0') {
            $phone = '234' . substr($phone, 1);
        }

        return $phone;
    }

}
