<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}


	/*
     * Download capitation list
     *
     * @access public
     * @param int $id_user
     * @return mixed (bool | array)
     * */

    public function download_capitation_list($record, $hospital, $name = 'download') {
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle($hospital." Capitation List");
        $objPHPExcel->getProperties()->setSubject($hospital." Capitation List");
        $objPHPExcel->getProperties()->setDescription($hospital." Capitation List");

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->setTitle("Capitation List");

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', "First Name");
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Last Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Other Names');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Address');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Phone');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Sex');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Marital Status');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Blood Group');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Genotype');

        $rowCount = 2;

        foreach ($record as $d) {
        	$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $d->first_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $d->surname);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $d->othernames);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $d->address);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $d->phone);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $d->email);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, get_gender($d->gender_id));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, get_marital_status($d->marital_status_id));
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, get_blood_group($d->blood_group_id));
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, get_genotype($d->genotype_id));

            $rowCount++;
        }



        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $name . '".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

	

}

/* End of file Excel.php */
/* Location: ./application/libraries/Excel.php */
