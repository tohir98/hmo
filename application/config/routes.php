<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|r
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
//$route['404_override'] = '';
//$route['translate_uri_dashes'] = FALSE;

$route['default_controller'] = 'user/User_controller/login';
//$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login']  = 'user/user_controller/login';
$route['logout']  = 'user/user_controller/logout';
$route['forgot_password'] = 'user/user_controller/forgot_password';
$route['user/change_password']  = 'user/user_controller/change_password';
$route['user/deleteUser']  = 'user/user_controller/deleteUser';
$route['user/deleteUser/(:num)']  = 'user/user_controller/deleteUser/$1';
$route['user/edit_user']  = 'user/user_controller/edit_user';


$route['admin']  = 'user/admin_controller';
$route['admin/(:any)']  = 'user/admin_controller/$1';
$route['admin/(:any)/(:any)']  = 'user/admin_controller/$1/$2';

// hospital routes
$route['hospitals']  = 'hospital/Hospital_controller/index';
$route['hospitals/(:any)']  = 'hospital/Hospital_controller/$1';
$route['hospitals/(:any)/(:any)']  = 'hospital/Hospital_controller/$1/$2';

// Plans Settings
$route['settings/plans']  = 'settings/plan_controller/index';
$route['settings/plans/(:any)']  = 'settings/plan_controller/$1';
$route['settings/plans/(:any)/(:any)']  = 'settings/plan_controller/$1/$2';
// Benefit Settings
$route['settings/all_benefits']  = 'settings/plan_controller/all_benefits';
$route['settings/benefits/(:num)']  = 'settings/plan_controller/all_benefits/$1';
$route['settings/benefit_categories']  = 'settings/plan_controller/benefit_categories';

$route['settings/messages']  = 'settings/settings_controller/messages';

$route['settings/hospital_class']  = 'settings/hospital_class_controller/index';

// Subscribers routes
$route['subscribers']  = 'subscribers/Subscribers_controller/index';
$route['subscribers/(:any)']  = 'subscribers/Subscribers_controller/$1';
$route['subscribers/(:any)/(:any)']  = 'subscribers/Subscribers_controller/$1/$2';


// Access control
$route['access_control']  = 'user/access_controller';
$route['access_control/(:any)']  = 'user/access_controller/$1';
$route['access_control/(:any)/(:any)']  = 'user/access_controller/$1/$2';

// Report
$route['report']  = 'report/report_controller';
$route['report/(:any)']  = 'report/report_controller/$1';
$route['report/(:any)/(:any)']  = 'report/report_controller/$1/$2';