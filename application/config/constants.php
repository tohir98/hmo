<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


// Tables
define('TBL_USERS', 'users'); 
define('TBL_USER_PERMS', 'user_perms'); 
define('TBL_PLANS', 'health_plans'); 
define('TBL_BENEFIT_CATEGORIES', 'benefit_categories'); 
define('TBL_BENEFITS', 'benefit'); 
define('TBL_PLAN_BENEFITS', 'health_plan_benefits'); 
define('TBL_HOSPITAL', 'hospitals'); 
define('TBL_HOSPITAL_CLASS', 'hospital_class'); 
define('TBL_ENROLMENT', 'enrollments'); 
define('TBL_SUBSCRIBER', 'subscribers'); 
define('TBL_ORG', 'organizations'); 
define('TBL_STATES', 'states'); 

//User status
define('USER_STATUS_INACTIVE', 0); 
define('USER_STATUS_ACTIVE', 1);

//statuses
define('INACTIVE', 0); 
define('ACTIVE', 1); 
define('APPROVED', 1); 


//User status
define('USER_TYPE_ADMIN', 1); 
define('USER_TYPE_ORDINARY', 2); 
define('USER_TYPE_SUBSCRIBER', 3); 

// Min password length
define('MIN_PWD_LEN', 6); 

//company
define('BUSINESS_NAME', 'Health Care Intl.'); 
define('BUSINESS_NAME_FULL', 'Health Care Intl.'); 
define('WEBSITE', 'http://healthcareinternational.com'); 
define('NO_REPLY_EMAIL', 'no-reply@healthcareinternational.com'); 
define('INFO_EMAIL', 'info@healthcareinternational.com'); 

define('FILE_PATH', FCPATH . 'files/');

define('FILE_PATH_HOSPITAL_LOGO', FILE_PATH . 'hospital_logos/');
if (!is_dir(FILE_PATH_HOSPITAL_LOGO)) {
    @mkdir(FILE_PATH_HOSPITAL_LOGO, 0777, true);
}

define('FILE_PATH_PROFILE', FILE_PATH . 'profiles/');
if (!is_dir(FILE_PATH_PROFILE)) {
    @mkdir(FILE_PATH_PROFILE, 0777, true);
}

define('FILE_PATH_ADS_IMG', FILE_PATH . 'ads_image/');
if (!is_dir(FILE_PATH_ADS_IMG)) {
    @mkdir(FILE_PATH_ADS_IMG, 0777, true);
}


// Default Logo Image
define('DEFAULT_LOGO', 'default_logo.png');


//Enrollment Types
define('SUBSCRIBER', 1);
define('DEPENDANTS', 2);

// Default welcome message
define('WELCOME_SMS', "An account has been opened for you at Healthcare International. Welcome on board");