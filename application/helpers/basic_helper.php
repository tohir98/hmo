<?php

/**
 * Get state/states
 *
 * @return void
 * @author 
 **/

function get_states($id = null){
	$CI = & get_instance();
    $CI->load->model('general_model');
    
    $st = $CI->general_model->all('states');
	$states_array = [];

    foreach ($st as $s) {
    	$states_array[$s->state_id] = $s->state;
    }

	return ($id) ? $states_array[$id] : $states_array ;
}


/**
 * Get gender
 *
 * @return void
 * @author 
 **/
function get_gender($id = null)
{
	$CI = & get_instance();
    $CI->load->model('general_model');
    
    $gender = $CI->general_model->all('gender');
	$gender_array = [];

    foreach ($gender as $g) {
    	$gender_array[$g->gender_id] = $g->gender;
    }

	return ($id) ? $gender_array[$id] : $gender_array ;
}



/**
 * Get user title
 *
 * @return void
 * @author 
 **/
function get_title($id = null)
{
	$CI = & get_instance();
    $CI->load->model('general_model');
    
    $titles = $CI->general_model->all('titles');
	$title_array = [];

    foreach ($titles as $t) {
    	$title_array[$t->title_id] = $t->title;
    }

	return ($id) ? $title_array[$id] : $title_array ;
}


/**
 * Get genotype
 *
 * @return void
 * @author 
 **/
function get_genotype($id = null)
{
	$CI = & get_instance();
    $CI->load->model('general_model');
    
    $genotypes = $CI->general_model->all('genotypes');
	$genotype_array = [];

    foreach ($genotypes as $g) {
    	$genotype_array[$g->genotype_id] = $g->genotype;
    }

	return ($id) ? $genotype_array[$id] : $genotype_array ;
}



/**
 * Get blood group
 *
 * @return void
 * @author 
 **/
function get_blood_group($id = null)
{
	$CI = & get_instance();
    $CI->load->model('general_model');
    
    $bloods = $CI->general_model->all('blood_groups');
	$blood_array = [];

    foreach ($bloods as $b) {
    	$blood_array[$b->blood_group_id] = $b->blood_group;
    }

	return ($id) ? $blood_array[$id] : $blood_array ;
}


/**
 * get marital status
 *
 * @return void
 * @author 
 **/
function get_marital_status($id = null)
{
    $CI = & get_instance();
    $CI->load->model('general_model');
    
    $maritals = $CI->general_model->all('marital_status');
    $marital_array = [];

    foreach ($maritals as $m) {
        $marital_array[$m->marital_status_id] = $m->marital_status;
    }

    return ($id) ? $marital_array[$id] : $marital_array ;
}


/**
 * get enrollment types
 *
 * @return void
 * @author 
 **/
function get_subscriber_type($id = null)
{
	$enrollment_array = [
        SUBSCRIBER => 'Subscriber',
        DEPENDANTS => 'Dependant'
    ];

	return ($id) ? $enrollment_array[$id] : $enrollment_array ;
}


/**
 * undocumented function
 *
 * @return void
 * @author 
 **/
function get_months($id = null)
{
    $month_array = [
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December'
    ];


    return ($id) ? $month_array[$id] : $month_array ;

}


/**
 * get capitation class
 *
 * @return void
 * @author 
 **/
function get_capitation_class($id = null)
{
    $CI = & get_instance();
    $CI->load->model('general_model');
    
    $capitations = $CI->general_model->all('hospital_class');
    $capitation_array = [];

    foreach ($capitations as $c) {
        $capitation_array[$c->id] = $c->class;
    }

    return ($id) ? $capitation_array[$id] : $capitation_array ;
}