<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Basic Model
 * 
 * @category   Model
 * @package    Basic
 * @subpackage Basic
 * @author     Obinna Ukpabi <askphntm@gmail.com>
 * @copyright  Copyright © 2015 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class General_model extends CI_Model {
	
    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();

    }// End func __construct



	/*
     * Get one
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function all($table, $limit = null){

        if(empty($table)){
            return false;
        }
        
        $this->db->limit($limit);
        $res = $this->db->get($table);
        return $res->result();
        
    }// End func get


    /*
     * Get count of query
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get_count($table, $where=Null){

        if(empty($table)){
            return false;
        }
        
        $res = $this->db->get_where($table, $where);
        return $res->num_rows();
        
    }// End func get


    /*
     * Get one
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get_one($table, $where=Null){

        if(empty($table)){
            return false;
        }
        
        $res = $this->db->get_where($table, $where);
        return $res->row();
        
    }// End func get


    /*
     * Get many
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get_many($table, $where=Null){

        if(empty($table)){
            return false;
        }
        
        $res = $this->db->get_where($table, $where);
        return $res->result();
        
    }// End func get



    /*
     * add
     * 
     * @access public 
     */
    public function add($table, $data, $id=false){

        if(empty($data) OR empty($table)){
            return false;
        }

        $q = $this->db->insert($table, $data);

        if($id){
            return $this->db->insert_id();
        }
        else{
             return $q;
        }
                
    }// End func


    /*
     * edit/update
     * 
     * @access public 
     */
    public function update($table, $data, $where = null){

        if(empty($data) OR empty($table)){
            return false;
        }
        
        $q = $this->db->update($table, $data, $where);
        return $q;

        
    }// End func



    /*
     * delete
     * 
     * @access public 
     */
    public function delete($table, $where){

        if(empty($where) OR empty($table)){
            return false;
        }
        
        $q = $this->db->delete($table, $where);
        return $q;

        
    }// End func


    /**
     * Insert batch
     * 
     * @access public
     * @param $table string, $data multi-dimensional array
     * @return void
     */    

    
    public function insert_batch($table, $data){

        if(empty($table) OR empty($data)){
            return false;
        }

        return $this->db->insert_batch($table, $data);
    }


    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function last_query()
    {
        return $this->db->last_query();
    }


    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function plans($id = null)
    {
        if(!$id){
            $plans = $this->db->get('health_plans')->result();
            if(!empty($plans)){
                foreach ($plans as $p) {
                    $p->benefits = $this->db->get_where('health_plan_benefits', ['health_plan_id', $p->health_plan_id])->result();
                }
            }
        }
        else{
            $plans = $this->db->get_where('health_plans', ['health_plan_id' => $id])->row();
            if(!empty($plans)){
                $plans->benefits = $this->db->get_where('health_plan_benefits', ['health_plan_id' => $plans->health_plan_id])->result();
            }
        }

        return $plans;
    }

}
