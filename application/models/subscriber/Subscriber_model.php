<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Subscriber Model
 * 
 * @category   Model
 * @package    Basic
 * @subpackage Basic
 * @author     Obinna Ukpabi <askphntm@gmail.com>
 * @copyright  Copyright © 2016 
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property CI_DB_query_builder $db Description
 */
class Subscriber_model extends CI_Model {

    /**
     * undocumented function
     *
     * @return void
     * @author 
     * */
    public function save_subscriber($subscriber_id = null) {
        if ($this->input->post()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('subscriber_type', 'Subscriber type', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules('title', 'Title', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules('first_name', 'first name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'last name', 'trim|required');
            $this->form_validation->set_rules('address', 'residential address', 'trim|required');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
//			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            $this->form_validation->set_rules('gender', 'address', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules('marital', 'marital status', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules('genotype', 'state', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules('blood_group', 'phone', 'trim|required|greater_than[0]');



            if ($this->form_validation->run() == FALSE) {
                $error = validation_errors();
                $uri = (isset($subscriber_id)) ? '/subscribers/add' : '/subscribers/edit/' . $subscriber_id;
                notify('danger', 'Please fill form correctly.<br>' . $error, site_url($uri));
            }

            if (!empty($_FILES['profile']['name'])) {
                $config['upload_path'] = FILE_PATH_PROFILE;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 1024;
                $config['max_width'] = 850;
                $config['max_height'] = 1024;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('profile')) {
                    $error = $this->upload->display_errors();
                    $uri = (!isset($subscriber_id)) ? '/subscribers/add' : '/subscribers/edit/' . $subscriber_id;
                    notify('danger', 'File upload error. <br/>' . $error, $uri);
                } else {
                    $upload_file = $this->upload->data();
                }
            }


            $post_data = [
                'subscriber_type_id' => $this->input->post('subscriber_type'),
                'title_id' => $this->input->post('title'),
                'first_name' => $this->input->post('first_name'),
                'surname' => $this->input->post('last_name'),
                'othernames' => $this->input->post('middle_name'),
                'address' => $this->input->post('address'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'gender_id' => $this->input->post('gender'),
                'marital_status_id' => $this->input->post('marital'),
                'genotype_id' => $this->input->post('genotype'),
                'blood_group_id' => $this->input->post('blood_group'),
            ];

            if ($_FILES['profile']['name']) {
                $post_data['profile_pic'] = $upload_file['file_name'];
            }

            if (!isset($subscriber_id)) {
                $post_data['created_by'] = $this->session->userdata('user_id');
                $post_data['created_at'] = date('Y-m-d h:i:s');
            }

            if ($this->input->post('subscriber_parent_id')) {
                $post_data['subscriber_parent_id'] = $this->input->post('subscriber_parent_id');
            }

            if (isset($subscriber_id)) {
                $query = $this->db->update('subscribers', $post_data, ['subscriber_id' => $subscriber_id]);
            } else {
                $query = $this->db->insert('subscribers', $post_data);
                $subscriber_id = $this->db->insert_id();
            }


            if ($query) {

                // get default messages from settings
                $messages = $this->db->get(Settings_model::TBL_MGS_SETTINGS)->row();
                if (!empty($messages)) {
                    // send email notification
                    $mail_data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'message' => $messages->sign_up_email
                    );

                    $msg = $this->load->view('email_templates/account_signup', $mail_data, true);

                    $this->mailer
                            ->sendMessage('Account Created | HealthCare Intl', $msg, $this->input->post('email'));
                    //send SMS notification
                    $message = "Hi " . $this->input->post('first_name') . ' ' . $this->input->post('last_name') . ", " . $messages->sign_up_sms;
                    SmsAdapter::sendSms($this->input->post('phone'), $message);
                }

                notify('success', 'Success! Subscriber saved successfully.', '/subscribers/details/' . $subscriber_id);
            } else {
                $uri = (isset($subscriber_id)) ? '/subscribers/add' : '/subscribers/edit/' . $subscriber_id;
                notify('error', 'Error! There was a problem processing your request.', $uri);
            }
        }
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     * */
    public function my_plan($id) {
        if (!is_numeric($id)) {
            return false;
        }

        $plan = $this->db
                        ->select('e.*, hp.health_plan, hp.description, hp.premium, h.hospital_name')
                        ->from('enrollments e')
                        ->join(TBL_PLANS . ' hp', 'e.health_plan_id=hp.health_plan_id')
                        ->join(TBL_HOSPITAL . ' h', 'h.hospital_id=e.hospital_id')
                        ->where('e.subscriber_id', $id)
                        ->where('e.is_deleted', 0)
                        ->get()->row();

        if (!empty($plan)) {
            $plan->benefits = $this->db->get_where(TBL_PLAN_BENEFITS, ['health_plan_id' => $plan->health_plan_id])->result();
        }


        return $plan;
    }

    /**
     * Add Health Plan
     *
     * @return void
     * @author 
     * */
    public function add_health_plan() {
        if ($this->input->post('enrollment_type')) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('subscriber_id', '', 'trim|required|numeric');
            $this->form_validation->set_rules('plan', 'health plan', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules('hospital', 'hospital', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules('enrollment_type', 'Enrollment type', 'trim|required|greater_than[0]');

            $sub_id = $this->input->post('subscriber_id');

            if ($this->form_validation->run() == FALSE) {
                $error = validation_errors();
                notify('danger', 'Please fill form correctly.<br>' . $error, site_url('/subscribers/' . $sub_id));
            }

            $post_data = [
                'subscriber_type_id' => 0,
                'health_plan_id' => $this->input->post('plan'),
                'subscriber_id' => $this->input->post('subscriber_id'),
                'hospital_id' => $this->input->post('hospital'),
                'enrollment_type_id' => $this->input->post('enrollment_type'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d h:i:s')
            ];

            $query = $this->db->insert('enrollments', $post_data);

            if ($query) {
                notify('success', 'Success! Subscriber added successfully.', '/subscribers/details/' . $sub_id);
            } else {
                notify('error', 'Error! There was a problem processing your request.', '/subscribers/details/' . $sub_id);
            }
        }
    }

    /**
     * Edit health plan
     *
     * @return void
     * @author 
     * */
    public function edit_health_plan() {
        if ($this->input->post('edit_enrollment_id')) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('subscriber_id', '', 'trim|required|numeric');
            $this->form_validation->set_rules('edit_enrollment_id', '', 'trim|required|numeric');
            $this->form_validation->set_rules('edit_plan', 'health plan', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules('edit_hospital', 'hospital', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules('edit_enrollment_type', 'Enrollment type', 'trim|required|greater_than[0]');

            $sub_id = $this->input->post('subscriber_id');

            if ($this->form_validation->run() == FALSE) {
                $error = validation_errors();
                notify('danger', 'Please fill form correctly.<br>' . $error, site_url('/subscribers/' . $sub_id));
            }

            $post_data = [
                'health_plan_id' => $this->input->post('edit_plan'),
                'hospital_id' => $this->input->post('edit_hospital'),
                'enrollment_type_id' => $this->input->post('edit_enrollment_type')
            ];

            $query = $this->db->update('enrollments', $post_data, ['enrollment_id' => $this->input->post('edit_enrollment_id')]);

            if ($query) {
                notify('success', 'Success! Subscriber health plan updated successfully.', '/subscribers/details/' . $sub_id);
            } else {
                notify('error', 'Error! There was a problem processing your request.', '/subscribers/details/' . $sub_id);
            }
        }
    }

    /**
     * Delete health plan
     *
     * @return void
     * @author 
     * */
    public function delete_health_plan() {
        if ($this->input->post('delete_enrollment_id')) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('subscriber_id', '', 'trim|required|numeric');
            $this->form_validation->set_rules('delete_enrollment_id', '', 'trim|required|numeric');

            $sub_id = $this->input->post('subscriber_id');

            if ($this->form_validation->run() == FALSE) {
                $error = validation_errors();
                notify('danger', 'Please fill form correctly.<br>' . $error, site_url('/subscribers/' . $sub_id));
            }

            $post_data = [
                'is_deleted' => 1
            ];

            $query = $this->db->update('enrollments', $post_data, ['enrollment_id' => $this->input->post('delete_enrollment_id')]);

            if ($query) {
                notify('success', 'Success! Subscriber health plan cancelled successfully.', '/subscribers/details/' . $sub_id);
            } else {
                notify('error', 'Error! There was a problem processing your request.', '/subscribers/details/' . $sub_id);
            }
        }
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     * */
    public function add_premium() {

        if ($this->input->post('premium')) {

            // print_r($this->input->post()); exit;
            $this->load->library('form_validation');
            $this->form_validation->set_rules('subscriber_id', '', 'trim|required|numeric');
            $this->form_validation->set_rules('health_plan_id', '', 'trim|required|numeric');
            $this->form_validation->set_rules('premium', 'premium', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules('start_date', 'start date', 'trim|required');
            $this->form_validation->set_rules('end_date', 'end date', 'trim|required');

            $sub_id = $this->input->post('subscriber_id');

            if ($this->form_validation->run() == FALSE) {
                $error = validation_errors();
                notify('danger', 'Please fill form correctly.<br>' . $error, site_url('/subscribers/details/' . $sub_id));
            }

            $post_data = [
                'health_plan_id' => $this->input->post('health_plan_id'),
                'subscriber_id' => $this->input->post('subscriber_id'),
                'amount' => $this->input->post('premium'),
                'date_paid' => date('Y-m-d h:i:s'),
                'start_date' => date('Y-m-d h:i:s', strtotime($this->input->post('start_date'))),
                'end_date' => date('Y-m-d h:i:s', strtotime($this->input->post('end_date'))),
                'created_at' => date('Y-m-d h:i:s')
            ];

            $query = $this->db->insert('premium_payments', $post_data);

            if ($query) {
                // notify subscriber
                $subscriberInfo = $this->db->get_where(TBL_SUBSCRIBER, ['subscriber_id' => $this->input->post('subscriber_id')])->row();

                if (!empty($subscriberInfo)) {
                    $mail_data = array(
                        'first_name' => $subscriberInfo->first_name,
                        'last_name' => $subscriberInfo->surname,
                        'start_date' => date('d-M-Y', strtotime($this->input->post('start_date'))),
                        'end_date' => date('d-M-Y', strtotime($this->input->post('end_date'))),
                        'amount' => $this->input->post('premium'),
                        'header' => 'Premium Payment'
                    );

                    $msg = $this->load->view('email_templates/premium_payment', $mail_data, true);
                    
                    $this->mailer
                            ->sendMessage('Premium Payment Notification | HealthCare Intl', $msg, $subscriberInfo->email);
                    
                    //send SMS notification
                    $message = "Hi " . $subscriberInfo->first_name. ", Your premium payment for the period of " . $mail_data['start_date'] . " to " .$mail_data['end_date'] . " has been received. Thank you.";
                    SmsAdapter::sendSms($subscriberInfo->phone, $message);
                }

                notify('success', 'Success! Subscriber premium payment added successfully.', '/subscribers/details/' . $sub_id);
            } else {
                notify('error', 'Error! There was a problem processing your request.', '/subscribers/details/' . $sub_id);
            }
        }
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     * */
    public function edit_premium() {
        if ($this->input->post('edit_premium_id')) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('edit_premium_id', '', 'trim|required|numeric');
            $this->form_validation->set_rules('edit_premium', 'premium', 'trim|required|greater_than[0]');
            $this->form_validation->set_rules('edit_start_date', 'start date', 'trim|required');
            $this->form_validation->set_rules('edit_end_date', 'end date', 'trim|required');

            $sub_id = $this->input->post('subscriber_id');

            if ($this->form_validation->run() == FALSE) {
                $error = validation_errors();
                notify('danger', 'Please fill form correctly.<br>' . $error, site_url('/subscribers/details/' . $sub_id));
            }

            $post_data = [
                'amount' => $this->input->post('edit_premium'),
                'start_date' => date('Y-m-d h:i:s', strtotime($this->input->post('edit_start_date'))),
                'end_date' => date('Y-m-d h:i:s', strtotime($this->input->post('edit_end_date')))
            ];


            $query = $this->db->update('premium_payments', $post_data, ['premium_id' => $this->input->post('edit_premium_id')]);

            if ($query) {
                notify('success', 'Success! Subscriber premium payment updated successfully.', '/subscribers/details/' . $sub_id);
            } else {
                notify('error', 'Error! There was a problem processing your request.', '/subscribers/details/' . $sub_id);
            }
        }
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     * */
    public function delete_premium() {
        if ($this->input->post('delete_premium_id')) {
            // print_r($this->input->post()); exit;
            $this->load->library('form_validation');
            $this->form_validation->set_rules('subscriber_id', '', 'trim|required|numeric');
            $this->form_validation->set_rules('delete_premium_id', '', 'trim|required|numeric');

            $sub_id = $this->input->post('subscriber_id');

            if ($this->form_validation->run() == FALSE) {
                $error = validation_errors();
                notify('danger', 'Please fill form correctly.<br>' . $error, site_url('/subscribers/details/' . $sub_id));
            }

            $post_data = [
                'is_deleted' => 1
            ];

            $query = $this->db->update('premium_payments', $post_data, ['premium_id' => $this->input->post('delete_premium_id')]);

            if ($query) {
                notify('success', 'Success! Subscriber premium payment deleted successfully.', '/subscribers/details/' . $sub_id);
            } else {
                notify('error', 'Error! There was a problem processing your request.', '/subscribers/details/' . $sub_id);
            }
        }
    }

    public function getSubscriberId($subscriber_id = null) {
        if (!is_null($subscriber_id) && $subscriber_id > 0) {
            $this->db->where('e.subscriber_id', $subscriber_id);
        }
        return $this->db->select('s.*, p.health_plan')
                        ->from(TBL_ENROLMENT . ' e')
                        ->join(TBL_SUBSCRIBER . ' s', 'e.subscriber_id=s.subscriber_id', 'left')
                        ->join(TBL_PLANS . ' p', 'e.health_plan_id=p.health_plan_id', 'left')
                        ->where('s.subscriber_type_id', SUBSCRIBER)
                        ->get()->result();
    }

    public function getSubscriberByHospitalId($hospital_id) {
        return $this->db->select('s.*, p.health_plan')
                        ->from(TBL_ENROLMENT . ' e')
                        ->join(TBL_SUBSCRIBER . ' s', 'e.subscriber_id=s.subscriber_id')
                        ->join(TBL_PLANS . ' p', 'e.health_plan_id=p.health_plan_id')
                        ->where('e.hospital_id', $hospital_id)
                        ->get()->result();
    }
    
    public function fetchOrgs() {
        return $this->db->get(TBL_ORG)->result();
    }
    public function fetchStates() {
        return $this->db->get(TBL_STATES)->result();
    }
    public function fecthCountries() {
        return $this->db->get(TBL_STATES)->result();
    }

}

/* End of file Subscriber_model.php */
/* Location: ./application/models/subscriber/Subscriber_model.php */