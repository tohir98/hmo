<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Hospital Model
 * 
 * @category   Model
 * @package    Basic
 * @subpackage Basic
 * @author     Obinna Ukpabi <askphntm@gmail.com>
 * @copyright  Copyright © 2016
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property CI_DB_query_builder $db Description
 */
class Hospital_model extends CI_Model {

    /**
     * undocumented function
     *
     * @return void
     * @author 
     * */
    public function add_hospital() {
        if ($this->input->post()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('hospital_name', 'hospital', 'trim|required');
            $this->form_validation->set_rules('address1', 'address', 'trim|required');
            $this->form_validation->set_rules('city', 'city', 'trim|required');
            $this->form_validation->set_rules('state_id', 'state', 'trim|required');
            $this->form_validation->set_rules('contact_name', 'contact name', 'trim|required');
            $this->form_validation->set_rules('contact_phone', 'contact phone', 'trim|required');
            $this->form_validation->set_rules('contact_email', 'contact email', 'trim|required');


            if ($this->form_validation->run() == FALSE) {
                $error = validation_errors();
                notify('danger', 'Please fill form correctly.<br>' . $error, site_url('/hospitals'));
            }

            if (!empty($_FILES['logo']['name'])) {
                $config['upload_path'] = FILE_PATH_HOSPITAL_LOGO;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 1024;
                $config['max_width'] = 850;
                $config['max_height'] = 1024;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('logo')) {
                    $error = $this->upload->display_errors();
                    notify('danger', 'File upload error. <br/>' . $error, '/hospitals');
                } else {
                    $upload_file = $this->upload->data();
                }
            }


            $post_data = [
                'hospital_name' => $this->input->post('hospital_name'),
                'address1' => $this->input->post('address1'),
                'city' => $this->input->post('city'),
                'state_id' => $this->input->post('state_id'),
                'capitation_id' => $this->input->post('capitation_id'),
                'logo' => (isset($upload_file)) ? $upload_file['file_name'] : DEFAULT_LOGO,
                'created_by' => $this->session->userdata('user_id'),
                'contact_name' => $this->input->post('contact_name'),
                'contact_phone' => $this->input->post('contact_phone'),
                'contact_email' => $this->input->post('contact_email'),
                'created_at' => date('Y-m-d h:i:s')
            ];

            $query = $this->db->insert('hospitals', $post_data);

            if ($query) {
                $hospital_id = $this->db->insert_id();
                notify('success', 'Success! Hospital added successfully.', '/hospitals/details/' . $hospital_id);
            } else {
                notify('danger', 'Error! There was a problem processing your request.', '/hospitals');
            }
        }
    }



    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function get_hospital_subscriber($hospital_id)
    {

        $this->db->select('s.*');
        $this->db->from('subscribers s');
        $this->db->join(TBL_ENROLMENT .' as e', 'e.subscriber_id=s.subscriber_id');
        $this->db->where('s.subscriber_type_id', SUBSCRIBER);
        $this->db->where('e.hospital_id', $hospital_id);
        return $this->db->get()->result();
    }

    public function fetchHospitals() {
        return $this->db->select('h.*, hc.*')
                        ->from(TBL_HOSPITAL . ' as h')
                        ->join(TBL_HOSPITAL_CLASS . ' as hc', 'hc.id=h.capitation_id', 'left')
                        ->get()->result();

    }

}

/* End of file Hospital_model.php */
/* Location: ./application/models/hospital/Hospital_model.php */