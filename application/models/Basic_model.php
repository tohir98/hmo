<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of basic_model
 *
 * @author TOHIR
 */
class Basic_model extends CI_Model {
    
   

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Fetch records from table
     * @param type $table table name
     * @param type $where array
     * @param type $rowClass
     * @return boolean
     */
    public function fetch_all_records($table, $where = NULL, $rowClass = stdClass::class) {

        if (!$this->db->table_exists($table)) {
            trigger_error("Table `" . $table . "` not exists.", E_USER_ERROR);
        }

        if (is_null($where)) {
            $result = $this->db->get($table)->result($rowClass);
        } else {
            $result = $this->db->get_where($table, $where)->result($rowClass);
        }

        if (empty($result)) {
            return false;
        }

        return $result;
    }
   
    
    private function _sendEnquiryEmail($params) {
        $mail_data = array(
            'header' => 'Enquiry',
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email'],
            'phone' => $params['phone'],
            'message' => $params['message'],
        );

        $msg = $this->load->view('email_templates/enquiry', $mail_data, true);

        return $this->mailer
                ->sendMessage("Enquiry from ". $params['first_name'], $msg, INFO_EMAIL);
    }

    private function _recordExist($email) {
        return $this->db->get_where('student_event_reg', ['email' => $email])->result();
    }

    public function delete($table, $array) {
        $this->db->where($array);
        $q = $this->db->delete($table);
        return $q;
    }
    
    

    public function update($table, $data, $where) {
        foreach ($where as $k => $v) {
            $this->db->where($k, $v);
        }
        $query = $this->db->update($table, $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function insert($table, $data) {
        return $this->db->insert($table, $data);
        //return $this->db->insert_id();
    }

}
