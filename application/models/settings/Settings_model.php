<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Setup_model
 *
 * @author tohir
 * @property User_auth_lib $user_auth_lib Description
 * @property CI_DB_query_builder $db Description
 */
class Settings_model extends CI_Model {

    const TBL_MGS_SETTINGS = 'message_settings';
    const TBL_HOSPITAL_CLASS = 'hospital_class';

    static $default_email = "Welccome to HealthCare International, we are simply the best";
    static $default_sms = "An account has been opened for you at Healthcare International. Welcome on board";

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function saveDefaultMessage() {
        if (!$this->db->get(self::TBL_MGS_SETTINGS)->result()) {
            $this->db->insert(self::TBL_MGS_SETTINGS, [
                'sign_up_email' => static::$default_email,
                'sign_up_sms' => static::$default_sms,
                'date_added' => date('Y-m-d H:i:s'),
                'added_by' => $this->user_auth_lib->get('user_id'),
            ]);
        }
    }

    public function savePlan($data) {
        if (empty($data)) {
            return FALSE;
        }

        $data['created_by'] = $this->user_auth_lib->get('user_id');
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert(TBL_PLANS, $data);
    }
    
    public function saveBenefitCategory($data) {
        if (empty($data)) {
            return FALSE;
        }

//        $data['created_by'] = $this->user_auth_lib->get('user_id');
//        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert(TBL_BENEFIT_CATEGORIES, $data);
    }

    public function saveBenefit($data) {
        if (empty($data)) {
            return FALSE;
        }
        $data['created_by'] = $this->user_auth_lib->get('user_id');
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert(TBL_PLAN_BENEFITS, $data);
    }


    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function add_hospital_class()
    {
        if($this->input->post('capitation')){

            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'name', 'trim|required');
            $this->form_validation->set_rules('description', 'description', 'trim|required');
            $this->form_validation->set_rules('capitation', 'capitation', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $error = validation_errors();
                notify('danger', 'Please fill form correctly.<br>' . $error, '/settings/hospital_class');
            }

            $post_data = [
                'class' => $this->input->post('name'),
                'description' => $this->input->post('description'),
                'capitation' => $this->input->post('capitation'),
                'created_by' => $this->session->userdata('user_id'),
                'date_created' => date('Y-m-d h:i:s')
            ];

            $id = $this->input->post('id');

            if ($id > 0) {
                $q = $this->db->update('hospital_class', $post_data, ['id'=>$id]);
            } else {
                $q = $this->db->insert('hospital_class', $post_data);
            }
            
            if($q){
                 notify('success', 'Success! Hospital class has been created/updated.', '/settings/hospital_class');
            }
            else{
                 notify('danger', 'Error! There was a problem processing your request.', '/settings/hospital_class');
            }

        }
    }



    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function delete_hospital_class()
    {
        if($this->input->post('id_delete')){

            $id = $this->input->post('id_delete');
            $hclass = $this->db->get_where('hospital_class', ['id'=>$id])->row();

            if(empty($hclass)){
                 notify('danger', 'Error! Invalid parameter provided.', '/settings/hospital_class');
            }

            $hospitals = $this->db->get_where('hospitals', ['id_class'=>$id])->num_rows();
            if($hospitals > 0){
                 notify('danger', 'Error! '.$hospitals.' hospitals belong to this class. Kindly remove them first before deleting this class.', '/settings/hospital_class');
            }

            $q = $this->db->delete('hospital_class', ['id'=>$id]);
           
            if($q){
                 notify('success', 'Success! Hospital class has been deleted successfully.', '/settings/hospital_class');
            }
            else{
                 notify('danger', 'Error! There was a problem processing your request.', '/settings/hospital_class');
            }

        }
    }
    
    
    public function saveOrganization($data) {
        if (empty($data)) {
            return FALSE;
        }
        $data['created_by'] = $this->user_auth_lib->get('user_id');
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->db->insert(TBL_ORG, $data);
    }

}
