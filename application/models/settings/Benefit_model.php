<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Benefit_model
 *
 * @author tohir
 * @property CI_DB_query_builder $db Description
 */
class Benefit_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetchBenefits($id) {
        if ($id){
            $this->db->where('b.benefit_category_id', $id);
        }
        return $this->db->select('b.*, bc.category_name')
                        ->from(TBL_BENEFITS . ' as b')
                        ->join(TBL_BENEFIT_CATEGORIES . ' as bc', 'bc.benefit_category_id=b.benefit_category_id')
                        ->get()->result();
    }
    
    public function fetchPlanBenefits($plan_id = null) {
        if ($plan_id){
            $this->db->where('pb.health_plan_id', $plan_id);
        }
        
        return $this->db->select('pb.*, b.benefit, b.benefit_desc')
                ->from(TBL_PLAN_BENEFITS . ' as pb')
                ->join(TBL_BENEFITS . ' as b', 'pb.benefit_id=b.benefit_id')
                ->get()->result();
    }

}
