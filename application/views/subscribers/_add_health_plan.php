<div class="modal hide fade" id="add_plan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Add Health Plan</h4>
    </div>

    <form method="post"  enctype="multipart/form-data">
        <div class="modal-body nopadding">
            <table class="table table-striped">
                <tr>
                    <td class="text-right">Enrollment type</td>
                    <td>
                        <select name="enrollment_type" required="required">
                            <option value="0">Select Enrollment Type</option>
                            <?php foreach ($enrollments as $type_id => $enrollment_type) { ?>
                                <option value="<?= $type_id; ?>"><?= $enrollment_type; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="text-right">Plan</td>
                    <td>
                        <select name="plan" required="required">
                            <option value="0">Select Plan</option>
                            <?php foreach ($plans as $pl) { ?>
                                <option value="<?= $pl->health_plan_id; ?>"><?= $pl->health_plan; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="text-right">Hospital</td>
                    <td>
                        <select name="hospital" required="required">
                            <option value="0">Select Hospital</option>
                            <?php foreach ($hospitals as $h) { ?>
                                <option value="<?= $h->hospital_id; ?>"><?= $h->hospital_name; ?></option>
                            <?php } ?>
                        </select>
                        <input type="hidden" name="subscriber_id" value="<?= $subscriber->subscriber_id; ?>">
                    </td>
                </tr>	
            </table>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
            <button class="btn btn-primary" type="submit">Add Health Plan</button>
        </div>
    </form>
</div>