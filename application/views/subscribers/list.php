<?php echo show_notification(); ?>
<div class="page-header">
    <div class="pull-left">
        <h1>Enrollments</h1>
    </div>
    <div class="clearfix"></div>
</div>


<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered">
            <?php include '_tab.php'; ?>
            <div class="box-title">
                <h3>
                    <i class="icon-bar-chart"></i>
                    Enrollments
                </h3>
                <?php if ($hospitals > 0): ?>
                    <a href="subscribers/add" data-toggle="modal" class="btn btn-success pull-right" style="margin-right: 5px">
                        <i class="icons icon-plus-sign"></i> New Subscriber
                    </a>	
                <?php endif ?>

            </div>
            <div class="box-content-padless">

                <?php include '_subscribers.php'; ?>

            </div>
        </div>
    </div>
</div>


<div class="modal hide fade" id="add_subscriber" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Add New Enrollment</h4>
    </div>


</div>