<?php echo show_notification(); ?>
<div class="row-fluid">
	<div class="container">
	    <div class="span12">
			<h1>Enrollments</h1>
	    </div>	    
	</div>
</div>


<div class="row-fluid">
	<div class="container">
	    <div class="span2">
			<a class="btn btn-warning" href="/subscribers/details/<?=$subscriber->subscriber_parent_id;?>">
                <i class="icons icon-chevron-left"></i> Back
            </a>
	    </div>	 
	</div>
</div>

<p>&nbsp;</p>

<div class="row-fluid">
	<div class="container">
		<div class="span12">
			<ul class="nav nav-tabs" id="myTab">
			    <li class="active">
			        <a href="#">Subscriber Profile</a>
			    </li>
			</ul>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="container">
		<div class="span3">
			<div class="box">
				<div class="box-title" style="border:1px solid #CCCCCC; padding:0;">
					<div class="row-fluid">
						<?php 
							if($subscriber->profile_pic != '') { 
								$logo_url = "/files/profiles/".$subscriber->profile_pic;
							} else {
								$logo_url = '/files/profiles/default-logo.jpg';
							}
						?>
						<div class="img">
							<img src="<?php echo $logo_url; ?>" />
						</div>
					</div>
						
				</div>
				<div class="_box-content">
					
					<div class="span12">
						<div class="box box-bordered">
			                <div class="box-title">
			                    <h3>Health Plan</h3>
			                </div>
			                <div class="box-content-padless">
			                <?php if (empty($plan)): ?>
			                	<?= show_no_data('There is no health plan added yet.')  ?>
			                <?php else: ?>
			                	<table class="table table-striped">
									<tr>
										<td><strong>Health Plan</strong></td>
										<td><?=$plan->health_plan;?></td>
									</tr>
									<tr>
										<td><strong>Description</strong></td>
										<td><?=$plan->description;?></td>
									</tr>
									<tr>
										<td><strong>Premium</strong></td>
										<td><?=$plan->premium;?></td>
									</tr>
									<tr>
										<td><strong>Date Created</strong></td>
										<td><?=date('d-M-Y', strtotime($plan->created_at));?></td>
									</tr>
									<tr>
										<td><strong>Benefits</strong></td>
										<td>
											<ul>
												<?php if (!empty($plan->benefits)): ?>
													<?php foreach ($plan->benefits as $b): ?>
														<li><?=$b->plan_benefit;?></li>
													<?php endforeach ?>
												<?php endif ?>
												
											</ul>
										</td>
									</tr>
									
								</table>
			                <?php endif ?>
								
							</div>
						</div>
					</div>
												
				</div>
			</div>
			
		</div>
		<div class="span9">
			<div class="row-fluid">
				
			    <div class="span12">
			    	<div class="box box-bordered">
		                <div class="box-title">
		                    <h3>
		                        <i class="icon-bar-chart"></i>
		                        Details
		                    </h3>
		                    <a href="/subscribers/edit/<?=$subscriber->subscriber_id;?>" class="btn btn-primary pull-right" style="margin-right:10px;">Edit</a>
		                </div>
		                <div class="box-content-padless">
							<table class="table table-striped table-bordered">
								<tr>
									<td width="40%">Enrollment Type</td>
									<td><span class="label"><?=get_subscriber_type($subscriber->subscriber_type_id);?></span></td>
								</tr>
								<tr>
									<td width="40%">Surname</td>
									<td><strong><?=$subscriber->surname;?></strong></td>
								</tr>
								<tr>
									<td>Names</td>
									<td><strong><?=$subscriber->first_name." ".$subscriber->othernames;?></strong></td>
								</tr>
								<tr>
									<td>Address</td>
									<td><?=$subscriber->address;?></td>
								</tr>
								<tr>
									<td>Phone</td>
									<td><?=$subscriber->phone;?></td>
								</tr>
								<tr>
									<td>Email</td>
									<td><?=$subscriber->email;?></td>
								</tr>
								<tr>
									<td>Gender</td>
									<td><?= get_gender($subscriber->gender_id);?></td>
								</tr>
								<tr>
									<td>Marital Status</td>
									<td><?= get_marital_status($subscriber->marital_status_id);?></td>
								</tr>
								<tr>
									<td>Blood Group</td>
									<td><?= get_blood_group($subscriber->blood_group_id);?></td>
								</tr>
								<tr>
									<td>Genotype</td>
									<td><?= get_genotype($subscriber->genotype_id);?></td>
								</tr>
							</table>
							
						</div>
					</div>
					
					<div class="box box-bordered">
		                <div class="box-title">
		                    <h3>
		                        <i class="icon-bar-chart"></i>
		                        Provider
		                    </h3>
		                </div>
		                <div class="box-content-padless">
						
							<table class="table table-striped">
						    	<tbody>
									
									<tr>
					                    <td>
					                        <?php 
					                            if($provider->profile_pic != '') { 
					                                $logo_url = "/files/profiles/".$provider->profile_pic;
					                            } else {
					                                $logo_url = '/files/profiles/default-logo.jpg';
					                            }
					                        ?>
					                        <div class="img">
					                            <img src="<?php echo $logo_url; ?>" style="height: 30px;" />
					                        </div>
					                    </td>
						    			<td>
					                        <a href="/subscribers/details/<?=$provider->subscriber_id;?>">
					                            <?=$provider->first_name." ".$provider->othernames." ".$provider->surname;?>
					                        </a>
					                    </td>
						    			<td><?=$provider->address;?></td>
					                    <td><?=$provider->phone;?></td>
						    		</tr>
						    		
						    	</tbody>
						    </table>
							
						</div>
					</div>
			    </div>
				
			</div>
		</div>
	</div>
</div>
