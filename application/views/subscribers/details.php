<?php echo show_notification(); ?>
<div class="page-header">
    <div class="pull-left">
        <h1>Enrollments</h1>
    </div>
    <div class="clearfix"></div>
</div>


<div class="row-fluid">
    <div class="span2">
        <a class="btn btn-warning" href="<?= $this->input->server('HTTP_REFERER') ? : site_url('/subscribers'); ?>">
            <i class="icons icon-chevron-left"></i> Back
        </a>
    </div>	

    <div class="span10">
        <select id="subscriberlist" class="select2-me input-xxlarge">
            <?php
            if (!empty($subscribers)) {
                $sel = '';
                foreach ($subscribers as $sub) {
                    if ($sub->subscriber_id == $subscriber->subscriber_id) {
                        $sel = 'selected';
                    } else {
                        $sel = '';
                    }
                    ?>
                    <option value="<?php echo $sub->subscriber_id; ?>" <?php echo $sel; ?>><?php echo ucfirst($sub->first_name . ' ' . $sub->surname) ?></option>
                    <?php
                }
            }
            ?>
        </select>
    </div>   
</div>

<p>&nbsp;</p>

<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-tabs" id="myTab">
            <li class="active">
                <a href="<?= site_url('/subscribers/' . $subscriber->subscriber_id); ?>">Subscriber Profile</a>
            </li>
        </ul>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <div class="box">
            <div class="box-title" style="border:1px solid #CCCCCC; padding:0;">
                <div class="row-fluid">
                    <?php
                    if ($subscriber->profile_pic != '') {
                        $logo_url = "/files/profiles/" . $subscriber->profile_pic;
                    } else {
                        $logo_url = '/files/profiles/default-logo.jpg';
                    }
                    ?>
                    <div class="img">
                        <img src="<?php echo $logo_url; ?>" />
                    </div>
                </div>

            </div>
            <div class="_box-content">

                <div class="span12">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td width="40%">Enrollment Type</td>
                            <td><span class="label"><?= get_subscriber_type($subscriber->subscriber_type_id); ?></span></td>
                        </tr>
                        <tr>
                            <td width="40%">Surname</td>
                            <td><strong><?= $subscriber->surname; ?></strong></td>
                        </tr>
                        <tr>
                            <td>Names</td>
                            <td><strong><?= $subscriber->first_name . " " . $subscriber->othernames; ?></strong></td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td><?= $subscriber->address; ?></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><?= $subscriber->phone; ?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><?= $subscriber->email; ?></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td><?= get_gender($subscriber->gender_id); ?></td>
                        </tr>
                        <tr>
                            <td>Marital Status</td>
                            <td><?= get_marital_status($subscriber->marital_status_id); ?></td>
                        </tr>
                        <tr>
                            <td>Blood Group</td>
                            <td><?= get_blood_group($subscriber->blood_group_id); ?></td>
                        </tr>
                        <tr>
                            <td>Genotype</td>
                            <td><?= get_genotype($subscriber->genotype_id); ?></td>
                        </tr>
                    </table>
                    <p></p>
                    <a href="/subscribers/edit/<?= $subscriber->subscriber_id; ?>" class="btn btn-primary">Edit</a>
                </div>

            </div>
        </div>

    </div>
    <div class="span9">
        <div class="row-fluid">

            <div class="span12">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3>
                            <i class="icon-bar-chart"></i>
                            Health Plan
                        </h3>
                        <?php if ((!empty($plans) || !empty($hositals)) && empty($myplan)): ?>
                            <a href="#add_plan" data-toggle="modal" class="btn btn-success pull-right" style="margin-right: 5px">
                                <i class="icons icon-plus-sign"></i> New Plan
                            </a>
                        <?php else: ?>
                            <a href="#delete_plan" data-toggle="modal" class="btn btn-warning pull-right" style="margin-right: 5px">
                                <i class="icons icon-trash"></i> Cancel Plan
                            </a>
                            <a href="#edit_plan" data-toggle="modal" class="btn btn-primary pull-right" style="margin-right: 5px">
                                <i class="icons icon-edit"></i> Edit Plan
                            </a>


                        <?php endif ?>

                    </div>
                    <div class="box-content-padless">
                        <?php 
                        if (empty($myplan)): ?>
                            <?php echo show_no_data("Hi, There are no plans created yet. Please add health plans first."); ?>
                        <?php else: ?>

                            <table class="table table-striped">
                                <tr>
                                    <td><strong>Hospital</strong></td>
                                    <td><?= $myplan->hospital_name; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Health Plan</strong></td>
                                    <td><?= $myplan->health_plan; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Description</strong></td>
                                    <td><?= $myplan->description; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Premium</strong></td>
                                    <td><?= number_format($myplan->premium, 2); ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Date Created</strong></td>
                                    <td><?= date('d-M-Y', strtotime($myplan->created_at)); ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Benefits</strong></td>
                                    <td>
                                        <ul>
                                            <?php if (!empty($myplan->benefits)): ?>
                                                <?php foreach ($myplan->benefits as $b): ?>
                                                    <li><?= $b->plan_benefit; ?></li>
                                                <?php endforeach ?>
                                            <?php endif ?>

                                        </ul>
                                    </td>
                                </tr>

                            </table>
                        <?php endif ?>

                    </div>
                </div>

                <div class="box box-bordered">
                    <div class="box-title">
                        <h3>
                            <i class="icon-bar-chart"></i>
                            Dependants
                        </h3>
                        <?php if (!empty($myplan)): ?>
                            <a href="/subscribers/add_dependant/<?= $subscriber->subscriber_id; ?>" data-toggle="modal" class="btn btn-success pull-right" style="margin-right: 5px">
                                <i class="icons icon-plus-sign"></i> Add Dependants
                            </a>
                        <?php endif; ?>			                 
                    </div>
                    <div class="box-content-padless">
                        <?php if (empty($dependants)): ?>
                            <?php echo show_no_data("Hi, There are no dependants added yet."); ?>
                        <?php else: ?>
                            <table class="table table-striped	">
                                <thead>
                                    <tr>
                                        <th>...</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($dependants as $d): ?>
                                        <tr>
                                            <td>
                                                <?php
                                                if ($d->profile_pic != '') {
                                                    $logo_url = "/files/profiles/" . $d->profile_pic;
                                                } else {
                                                    $logo_url = '/files/profiles/default-logo.jpg';
                                                }
                                                ?>
                                                <div class="img">
                                                    <img src="<?php echo $logo_url; ?>" style="height: 30px;" />
                                                </div>
                                            </td>
                                            <td>
                                                <a href="/subscribers/dependant/<?= $d->subscriber_id; ?>">
                                                    <?= ucwords($d->first_name . " " . $d->othernames . " " . $d->surname); ?>
                                                </a>
                                            </td>
                                            <td><?= $d->address; ?></td>
                                            <td><?= $d->phone; ?></td>
                                        </tr>
                                    <?php endforeach ?>

                                </tbody>
                            </table>
                        <?php endif ?>

                    </div>
                </div>

                <div class="box box-bordered">
                    <div class="box-title">
                        <h3>
                            <i class="icon-bar-chart"></i>
                            Recent Premium Payments
                        </h3>
                        <?php if (!empty($myplan)): ?>
                            <a href="#add_premium" data-toggle="modal" class="btn btn-success pull-right" style="margin-right: 5px">
                                <i class="icons icon-plus-sign"></i> Add Premium
                            </a>
                        <?php endif; ?>			                 
                    </div>
                    <div class="box-content-padless">
                        <?php if (empty($premiums)): ?>
                            <?php echo show_no_data("Hi, There are no premium payment added yet."); ?>
                        <?php else: ?>
                            <table class="table table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th>Date Paid</th>
                                        <th>Amount</th>
                                        <th>Payment For</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($premiums as $p): ?>
                                        <tr>
                                            <td><?= date('d-M-Y', strtotime($p->created_at)); ?></td>
                                            <td>NGN <?= number_format($p->amount, 2); ?></td>
                                            <td><?= date('d-M-Y', strtotime($p->start_date)); ?> to <?= date('d-M-Y', strtotime($p->end_date)); ?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-info pull-left dropdown-toggle">Action <span class="caret"></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" class="edit-premium" data-premium="<?= $p->amount; ?>" data-month="<?= date('d-m-Y', strtotime($p->start_date)); ?>" data-year="<?= date('d-m-Y', strtotime($p->end_date)); ?>" data-pid="<?= $p->premium_id; ?>">Edit</a></li>
                                                        <li><a href="#" class="delete-premium" data-id="<?= $p->premium_id; ?>">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>

                                </tbody>
                            </table>
                        <?php endif ?>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Add Health Plan -->
<?php include '_add_health_plan.php'; ?>

<?php if (!empty($enrollment)): ?>
    <!-- Edit Health Plan -->
    <div class="modal hide fade" id="edit_plan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Edit Health Plan</h4>
        </div>

        <form method="post"  enctype="multipart/form-data">
            <div class="modal-body nopadding">
                <table class="table table-striped">
                    <tr>
                        <td class="text-right">Enrollment type</td>
                        <td>
                            <select name="enrollment_type" required="required">
                                <option value="0">Select Enrollment Type</option>
                                <?php foreach ($enrollments as $type_id => $enrollment_type) { ?>
                                    <option value="<?= $type_id; ?>" <?= ($type_id == $enrollment->enrollment_type_id) ? 'selected' : ''; ?>><?= $enrollment_type; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Plan</td>
                        <td>
                            <select name="plan" required="required">
                                <option value="0">Select Plan</option>
                                <?php foreach ($plans as $pl) { ?>
                                    <option value="<?= $pl->health_plan_id; ?>" <?= ($pl->health_plan_id == $enrollment->health_plan_id) ? 'selected' : ''; ?>><?= $pl->health_plan; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Hospital</td>
                        <td>
                            <select name="hospital" required="required">
                                <option value="0">Select Hospital</option>
                                <?php foreach ($hospitals as $h) { ?>
                                    <option value="<?= $h->hospital_id; ?>" <?= ($h->hospital_id == $enrollment->hospital_id) ? 'selected' : ''; ?>><?= $h->hospital_name; ?></option>
                                <?php } ?>
                            </select>
                            <input type="hidden" name="edit_enrollment_id" value="<?= $enrollment->enrollment_id; ?>">
                            <input type="hidden" name="subscriber_id" value="<?= $subscriber->subscriber_id; ?>">
                        </td>
                    </tr>	
                </table>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
                <button class="btn btn-primary" type="submit">Edit Health Plan</button>
            </div>
        </form>
    </div>	

    <!-- Delete Health Plan -->
    <div class="modal hide fade" id="delete_plan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Cancel Health Plan</h4>
        </div>

        <form method="post">
            <div class="modal-body">
                <p>Are you sure you want to delete this health plan?</p>
                <input type="hidden" name="delete_enrollment_id" value="<?= $enrollment->enrollment_id; ?>">
                <input type="hidden" name="subscriber_id" value="<?= $subscriber->subscriber_id; ?>">
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
                <button class="btn btn-primary" type="submit">Delete Health Plan</button>
            </div>
        </form>
    </div>	
<?php endif ?>

<!-- Add Premium -->
<?php include '_premium_payment.php'; ?>

<!-- Edit Premium -->
<div class="modal hide fade" id="edit_premium" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Edit Premium</h4>
    </div>

    <form method="post"  enctype="multipart/form-data">
        <div class="modal-body nopadding">
            <table class="table table-striped">
                <tr>
                    <td><strong>Health Plan</strong></td>
                    <td><?= $myplan->health_plan; ?></td>
                </tr>
                <tr>
                    <td><strong>Description</strong></td>
                    <td><?= $myplan->description; ?></td>
                </tr>
                <tr>
                    <td><strong>Premium</strong></td>
                    <td><input type="number" name="edit_premium" value=""></td>
                </tr>
                <tr>
                    <td class="text-right"><strong>Start Date</strong></td>
                    <td><input type="text" name="edit_start_date" value="" class="datepick"></td>
                </tr>
                <tr>
                    <td class="text-right"><strong>End</strong></td>
                    <td>
                        <input type="text" name="edit_end_date" value="" class="datepick">
                        <input type="hidden" name="edit_premium_id" value="">
                        <input type="hidden" name="subscriber_id" value="<?= $subscriber->subscriber_id; ?>">
                    </td>
                </tr>	
            </table>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
            <button class="btn btn-primary" type="submit">Edit Premium</button>
        </div>
    </form>
</div>


<!-- Delete Premium -->
<div class="modal hide fade" id="delete_premium" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Add Premium</h4>
    </div>

    <form method="post">
        <div class="modal-body">

            <p>Are you sure you want to delete premium payment?</p>
            <input type="hidden" name="delete_premium_id" value="">
            <input type="hidden" name="subscriber_id" value="<?= $subscriber->subscriber_id; ?>">
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
            <button class="btn btn-primary" type="submit">Delete Premium Payment</button>
        </div>
    </form>
</div>




<script type="text/javascript">

    $('#subscriberlist').change(function () {
        var id_string = $(this).val();
        if (id_string.trim() === '') {
            return false;
        }
        window.location.href = '<?php echo site_url('/subscribers/details'); ?>/' + id_string;
    });


    $('body').on('click', '.edit-premium', function () {
        var premium = $(this).data('premium');
        var id_premium = $(this).data('pid');
        var month = $(this).data('month');
        var year = $(this).data('year');

        $('input[name="edit_premium"]').val(premium);
        $('input[name="edit_premium_id"]').val(id_premium);
        $('input[name="edit_start_date"]').val(month);
        $('input[name="edit_end_date"]').val(year);

        $('#edit_premium').modal('show');
    });

    $('body').on('click', '.delete-premium', function () {
        var id_premium = $(this).data('id');

        $('input[name="delete_premium_id"]').val(id_premium);
        $('#delete_premium').modal('show');
    });

</script>