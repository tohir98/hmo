<table class="table table-striped">
	<tr>
		<td class="text-right">Title</td>
		<td>
			<select name="title" required="required">
				<option value="0" selected>Select Title</option>
			<?php $titles = get_title();
				foreach ($titles as $title_id => $title) { ?>
					<?php $selected_title = (isset($subscriber) && $subscriber->title_id == $title_id) ? 'selected' : '' ;?>
					<option value="<?=$title_id;?>" <?=$selected_title;?>><?=$title;?></option>
			<?php }	?>
			</select>
		</td>
	</tr>
	<tr>
		<td class="text-right">First Name</td>
		<td><input type="text" name="first_name" placeholder="First Name" value="<?=(isset($subscriber)) ? $subscriber->first_name : '' ;?>" required="required"></td>
	</tr>
	<tr>
		<td class="text-right">Last Name</td>
		<td><input type="text" name="last_name" placeholder="Last Name" value="<?=(isset($subscriber)) ? $subscriber->surname : '' ;?>" required="required"></td>
	</tr>
	<tr>
		<td class="text-right">Middle Name</td>
		<td><input type="text" name="middle_name" placeholder="Middle Name" value="<?=(isset($subscriber)) ? $subscriber->othernames : '' ;?>"></td>
	</tr>
	<tr>
		<td class="text-right">Address</td>
		<td><input type="text" name="address" placeholder="Residential Address" value="<?=(isset($subscriber)) ? $subscriber->address : '' ;?>" required="required"></td>
	</tr>
	<tr>
		<td class="text-right">Phone</td>
                <td><input type="text" name="phone" placeholder="Phone Number" value="<?=(isset($subscriber)) ? $subscriber->phone : '' ;?>" maxlength="11" required="required"></td>
	</tr>
	<tr>
		<td class="text-right">Email</td>
		<td><input type="text" name="email" placeholder="Email Address" value="<?=(isset($subscriber)) ? $subscriber->email : '' ;?>"></td>
	</tr>
	<tr>
		<td class="text-right">Gender</td>
		<td>
			<select name="gender" required="required">
				<option value="0" selected>Select Gender</option>
				<?php $genders = get_gender();
				foreach ($genders as $gender_id => $gender) { ?>
					<?php $selected_gender = (isset($subscriber) && $subscriber->gender_id == $gender_id) ?  : 'selected' ;?>
					<option value="<?=$gender_id;?>" <?=$selected_gender;?>><?=$gender;?></option>
				<?php }	?>
			</select>
		</td>
	</tr>
	<tr>
		<td class="text-right">Marital Status</td>
		<td>
			<select name="marital" required="required">
				<option value="0" selected>Select Marital Status</option>
				<?php $marital = get_marital_status();
				foreach ($marital as $mstatus_id => $mstatus) { ?>
					<?php $selected = (isset($subscriber) && $subscriber->marital_status_id == $mstatus_id) ? 'selected' : '' ;?>
					<option value="<?=$mstatus_id;?>" <?=$selected;?>><?=$mstatus;?></option>
				<?php }	?>
			</select>
		</td>
	</tr>	
	<tr>
		<td class="text-right">Genotype</td>
		<td>
			<select name="genotype" required="required">
				<option value="0" selected>Select Genotype</option>
				<?php $genotypes = get_genotype();
				foreach ($genotypes as $genotype_id => $genotype) { ?>
					<?php $selected = (isset($subscriber) && $subscriber->genotype_id == $genotype_id) ? 'selected' : '' ;?>
					<option value="<?=$genotype_id;?>" <?=$selected;?>><?=$genotype;?></option>
				<?php }	?>
			</select>
		</td>
	</tr>
	<tr>
		<td class="text-right">Blood Group</td>
		<td>
			<select name="blood_group" required="required">
				<option value="0" selected>Select Blood Group</option>
				<?php $blood_groups = get_blood_group();
				foreach ($blood_groups as $blood_group_id => $blood_group) { ?>
					<?php $selected = (isset($subscriber) && $subscriber->blood_group_id == $blood_group_id) ? 'selected' : '' ;?>
					<option value="<?=$blood_group_id;?>" <?=$selected;?>><?=$blood_group;?></option>
				<?php }	?>
			</select>
		</td>
	</tr>
	<tr>
		<td class="text-right">Profile Pic</td>
		<td><input type="file" name="profile"></td>
	</tr><tr>
		<td class="text-right"></td>
		<td><button class="btn btn-primary" type="submit">Save Subscriber Details</button></td>
	</tr>
</table>    	