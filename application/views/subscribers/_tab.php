<?php $path = $this->input->server('REQUEST_URI'); ?>
<ul class="nav nav-tabs" id="myTab">
    <li class="<?= strstr($path, 'subscribers/index') ? 'active' : '' ?>">
        <a href="<?= site_url('/subscribers') ?>">Enrollees</a>
    </li>
    <li class="<?= strstr($path, '/subscribers/organizations') ? 'active' : '' ?>">
        <a href="<?= site_url('/subscribers/organizations') ?>">Organizations</a>
    </li>
</ul>
