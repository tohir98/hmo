<?php echo show_notification(); ?>
<div class="row-fluid">
	<div class="container">
	    <div class="span12">
			<h1>Edit Subscriber</h1>
	    </div>	    
	</div>
</div>

<div class="row-fluid">
	<div class="container">
	    <div class="span2">
			<a class="btn btn-warning" href="/subscribers">
                <i class="icons icon-chevron-left"></i> Back
            </a>
	    </div>
	</div>
</div>


<div class="row-fluid">
	<div class="container">
	    <div class="span12">
	    	<div class="box box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-bar-chart"></i>
                        Edit Subscriber
                    </h3>
                     
                </div>
                <div class="box-content-padless">
					<form method="post"  enctype="multipart/form-data">
						<input type="hidden" name="subscriber_type" value="<?= SUBSCRIBER; ?>">
						<?php $this->load->view('subscribers/_subscriber_form'); ?>
					</form>
				</div>
			</div>
	    </div>
	</div>
</div>