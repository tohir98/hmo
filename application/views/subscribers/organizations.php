<?php echo show_notification(); ?>
<div class="page-header">
    <div class="pull-left">
        <h1>Subscribers</h1>
    </div>
    <div class="clearfix"></div>
</div>


<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered">
            <?php include '_tab.php'; ?>
            <div class="box-title">
                <h3>
                    <i class="icon-bar-chart"></i>
                    Organizations
                </h3>
                <a href="#new_org" data-toggle="modal" class="btn btn-success pull-right" style="margin-right: 5px">
                    <i class="icons icon-plus-sign"></i> New Organization
                </a>

            </div>
            <div class="box-content-padless">

                <?php if (!empty($organizations)): ?>
                    <table class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>...</th>
                                <th>Organization</th>
                                <th>Contact Person</th>
                                <th>Contact Details</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sn=0; foreach ($organizations as $org): ?>

                                <tr>
                                    <td><?= ++$sn; ?></td>
                                    <td><?= $org->organization; ?></td>
                                    <td>
                                        <?= $org->contact_person; ?> <br>
                                        <span class="muted"><i class="icons icon-envelope"></i> <?= $org->contact_email; ?></span><br>
                                        <span class="muted"><i class="icons icon-phone"></i> <?= $org->contact_phone; ?></span>
                                    </td>
        <!--                                    <td>
                                    <?= $org->address; ?> <br>
                                        <span class="muted"> <?= $org->state_id; ?>, <?= $org->country; ?></span><br>
                                    </td>-->

                                    <td>
                                        <?= $org->address; ?>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="#" data-toggle="dropdown" class="btn btn-info pull-left dropdown-toggle">Action <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="/subscribers/edit_organization/<?= $org->organization_id; ?>">Edit</a></li>
                                                <li><a href="/subscribers/organization_details/<?= $org->organization_id; ?>">Delete Organization</a></li>
                                            </ul>
                                        </div>		
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                <?php else: ?>

                    <?php echo show_no_data('Hi, there are no organization data available. Please add new organization.'); ?>

                <?php endif ?>

            </div>
        </div>
    </div>
</div>

<?php include '_new_org.php'; ?>