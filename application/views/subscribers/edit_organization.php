<?php echo show_notification(); ?>
<div class="row-fluid">
	<div class="container">
	    <div class="span12">
			<h1>Edit Organization</h1>
	    </div>	    
	</div>
</div>

<div class="row-fluid">
	<div class="container">
	    <div class="span2">
			<a class="btn btn-warning" href="/subscribers/organizations">
                <i class="icons icon-chevron-left"></i> Back
            </a>
	    </div>
	</div>
</div>


<div class="row-fluid">
	<div class="container">
	    <div class="span12">
	    	<div class="box box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-bar-chart"></i>
                        Edit Organization
                    </h3>
                     
                </div>
                <div class="box-content-padless">
					<form method="post"  enctype="multipart/form-data">
						<input type="hidden" name="organization_type" value="<?= SUBSCRIBER; ?>">
						<?php $this->load->view('subscribers/_new_org'); ?>
					</form>
				</div>
			</div>
	    </div>
	</div>
</div>