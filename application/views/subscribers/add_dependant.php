<?php echo show_notification(); ?>
<div class="page-header">
    <div class="pull-left">
        <h1>
            Add New Dependant</h1>
    </div>
    <div class="clearfix"></div>
    <a class="btn btn-warning" href="/subscribers">
        <i class="icons icon-chevron-left"></i> Back
    </a>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered">
            <div class="box-title">
                <h3>
                    <i class="icon-bar-chart"></i>
                    New Dependant
                </h3>

            </div>
            <div class="box-content-padless">
                <form method="post"  enctype="multipart/form-data">
                    <input type="hidden" name="subscriber_type" value="<?= DEPENDANTS; ?>">
                    <input type="hidden" name="subscriber_parent_id" value="<?= $subscriber_id; ?>">
                    <?php include '_subscriber_form.php'; ?>
                </form>
            </div>
        </div>
    </div>
</div>