<?php if (!empty($subscribers)): ?>
    <table class="table table-striped dataTable">
        <thead>
            <tr>
                <th>...</th>
                <th>Name</th>
                <th>Contact</th>
                <th>Plan</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($subscribers as $sub): ?>
                <tr>
                    <td>
                        <?php
                        if ($sub->profile_pic != '') {
                            $logo_url = "/files/profiles/" . $sub->profile_pic;
                        } else {
                            $logo_url = '/files/profiles/default-logo.jpg';
                        }
                        ?>
                        <div class="img">
                            <img src="<?php echo $logo_url; ?>" style="width: 80px; height: 80px" />
                        </div>
                    </td>
                    <td>
                        <a href="/subscribers/details/<?= $sub->subscriber_id; ?>">
                            <?= $sub->first_name . " " . $sub->othernames . " " . $sub->surname; ?>
                        </a>
                    </td>
                    <td><?= $sub->address; ?> <br>
                        <span class="muted"><i class="icons icon-phone"></i> <?= $sub->phone; ?></span><br>
                        <span class="muted"><i class="icons icon-envelope"></i> <?= $sub->email; ?></span>
                    
                    </td>
                    <td><?= $sub->health_plan; ?></td>
                    <td>
                        <div class="btn-group">
                            <a href="#" data-toggle="dropdown" class="btn btn-info pull-left dropdown-toggle">Action <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/subscribers/edit/<?= $sub->subscriber_id; ?>">Edit</a></li>
                                <li><a href="/subscribers/details/<?= $sub->subscriber_id; ?>">View Details</a></li>
                            </ul>
                        </div>		
                    </td>
                </tr>
            <?php endforeach ?>

        </tbody>
    </table>

<?php else: ?>

    <?php echo show_no_data('Hi, there are no subscribers data available. Please add new subscribers data.'); ?>

<?php endif ?>