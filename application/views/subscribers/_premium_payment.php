<div class="modal hide fade" id="add_premium" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Add Premium</h4>
    </div>

    <form method="post" action="">
        <div class="modal-body nopadding">
            <table class="table table-striped">
                <tr>
                    <td><strong>Health Plan</strong></td>
                    <td><?= $myplan->health_plan; ?></td>
                </tr>
                <tr>
                    <td><strong>Description</strong></td>
                    <td><?= $myplan->description; ?></td>
                </tr>
                <tr>
                    <td><strong>Premium</strong></td>
                    <td><input type="number" name="premium" value="<?= $myplan->premium; ?>"></td>
                </tr>
                <tr>
                    <td class="text-right"><strong>Start Date</strong></td>
                    <td><input type="text" name="start_date" value="" class="datepick" style="cursor: pointer"></td>
                </tr>
                <tr>
                    <td class="text-right"><strong>End</strong></td>
                    <td>
                        <input type="text" name="end_date" value="" class="datepick" style="cursor: pointer">
                        <input type="hidden" name="subscriber_id" value="<?= $subscriber->subscriber_id; ?>">
                        <input type="hidden" name="health_plan_id" value="<?= $myplan->health_plan_id; ?>">
                    </td>
                </tr>	
            </table>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
            <button class="btn btn-primary" type="submit">Add Premium</button>
        </div>
    </form>
</div>