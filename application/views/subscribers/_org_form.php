<div class="modal-body nopadding">
    <div class="control-group">
        <label for="organization" class="control-label">Organization</label>
        <div class="controls">
            <input required type="text" placeholder="" class='input-xlarge'  value="<?= isset($organizations->organization) ? $organizations->organization : ''; ?>" id="organization" name="organization">
        </div>
    </div>
    <div class="control-group">
        <label for="address" class="control-label">Address</label>
        <div class="controls">
            <input required type="text" placeholder="" class='input-xlarge'  value="<?= isset($organizations->address) ? $organizations->address : ''; ?>" id="address" name="address">
        </div>
    </div>

    <div class="control-group">
        <label for="state" class="control-label">State</label>
        <select required name="state_id" id="state_id" class="select2-me input-xlarge">
            <option value="">Select Category</option>
            <?php
            if (!empty($states)):
                $sel = '';
                foreach ($states as $state):
                    if (isset($state->state_id) && $state->state_id == $state->state_id):
                        $sel = 'selected';
                    else:
                        $sel = '';
                    endif;
                    ?>
                    <option value="<?= $state->state_id ?>" <?= $sel; ?>><?= $state->state; ?></option>
                    <?php
                endforeach;
            endif;
            ?>
        </select>
    </div>
    <div class="control-group">
        <label for="website" class="control-label">Website</label>
        <div class="controls">
            <input required type="text" placeholder="" class='input-xlarge'  value="<?= isset($organizations->website) ? $organizations->website : ''; ?>" id="website" name="website">
        </div>
    </div>
    
    <div class="control-group">
        <label for="contact_person" class="control-label">Contact Person</label>
        <div class="controls">
            <input required type="text" placeholder="" class='input-xlarge'  value="<?= isset($organizations->contact_person) ? $organizations->contact_person : ''; ?>" id="contact_person" name="contact_person">
        </div>
    </div>
    
    <div class="control-group">
        <label for="contact_email" class="control-label">Contact Email</label>
        <div class="controls">
            <input required type="text" placeholder="" class='input-xlarge'  value="<?= isset($organizations->contact_email) ? $organizations->contact_email : ''; ?>" id="contact_email" name="contact_email">
        </div>
    </div>
    
    <div class="control-group">
        <label for="contact_phone" class="control-label">Contact Phone</label>
        <div class="controls">
            <input required type="text" placeholder="" class='input-xlarge'  value="<?= isset($organizations->contact_phone) ? $organizations->contact_phone : ''; ?>" id="contact_phone" name="contact_phone">
        </div>
    </div>
</div>