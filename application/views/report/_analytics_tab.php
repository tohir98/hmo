<div class="page-header">
    <div class="pull-left">
        <h1>Analytics &AMP; Reports</h1>
    </div>
    <div class="clearfix"></div>
</div>

<!-- Display error/success message -->
<?= show_notification(); ?>

<p>
<ul class="nav nav-tabs" id="myTab">
    <?php $path = $this->input->server('REQUEST_URI'); ?>
    <li class="<?= strstr($path, "report/analytics") ? 'active' : '' ?>">
        <a href="<?= site_url('report/analytics') ?>">Analytics</a>
    </li>
    <li class="<?= strstr($path, "report") ? 'active' : '' ?>">
        <a href="<?= site_url('/report') ?>">Reports</a>
    </li>
</ul>
</p>