<div class="box">
    <?php include '_analytics_tab.php'; ?>
    <div class="box-content nopadding">


        <div class="tab-content"> 


            <div class="box box-bordered">
                <div class="box-title">
                    <h3>Reports</h3>
                </div>

                <div class="box-content nopadding" id="data_container">
                    <table class="table table-hover table-nomargin table-striped">
                        <tbody>
                            <tr>
                                <td>Enrollee</td>
                                <td> 
                                    <a href="<?= site_url('report') ?>">Run Report</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Hospital</td>
                                <td> 
                                    <a href="<?= site_url('report') ?>">Run Report</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Company/Subscriber</td>
                                <td> 
                                    <a href="<?= site_url('report') ?>">Run Report</a>
                                </td>
                            </tr>
                        </tbody>

                    </table>
                </div>
            </div>	

        </div>
    </div>    
</div>