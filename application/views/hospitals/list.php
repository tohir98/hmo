<?php echo show_notification(); ?>
<div class="page-header">
    <div class="pull-left">
        <h1>Hospitals</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered">
            <div class="box-title">
                <h3>
                    <i class="icon-bar-chart"></i>
                    Hospitals
                </h3>
                <?php if ($this->user_auth_lib->have_perm('setup:add_hospital')): ?>
                <a href="#add_hospital" data-toggle="modal" class="btn btn-success pull-right" style="margin-right: 5px">
                    <i class="icons icon-plus-sign"></i> New Hospital
                </a>
                <?php endif; ?>
            </div>
            <div class="box-content-padless">
                <?php if (!empty($hospitals)): ?>
                    <table class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>Hospital Name</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Contact</th>
                                <th>Class</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($hospitals as $h): ?>
                                <tr>
                                    <td>
                                        <a href="/hospitals/details/<?= $h->hospital_id; ?>">
                                            <?= $h->hospital_name; ?>
                                        </a>
                                    </td>
                                    <td><?php echo $h->address1; ?> <br>
                                    <?php echo $h->city; ?>, <?php echo get_states($h->state_id); ?>
                                    </td>
                                    <td><?php echo $h->phone; ?></td>
                                    <td>
                                        <?php echo $h->contact_name; ?><br>
                                        <i class="icons icon-phone"></i> <?php echo $h->contact_phone; ?><br>
                                        <i class="icons icon-envelope"></i> <?php echo $h->contact_email; ?>
                                    
                                    </td>
                                    <td><?php echo $h->class; ?> <br>
                                        <span class="muted"><?= number_format($h->capitation, 2); ?></span>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="#" data-toggle="dropdown" class="btn btn-primary pull-left dropdown-toggle">Action <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <?php if ($this->user_auth_lib->have_perm('setup:edit_hospital')): ?>
                                                    <li><a class="edit_hospital" href="/hospitals/edit/<?= $h->hospital_id; ?>">Edit</a></li>
                                                <?php endif; ?>
                                                <li><a href="/hospitals/details/<?= $h->hospital_id; ?>">View Details</a></li>
                                                <li><a href="/hospitals/download_capitation_list/<?= $h->hospital_id; ?>">Download Capitation List</a></li>
                                            </ul>
                                        </div>		
                                    </td>
                                </tr>
                            <?php endforeach ?>

                        </tbody>
                    </table>

                <?php else: ?>

                    <?php echo show_no_data('Hi, there are no hospital data available. Please use the add button to add new hospital data.'); ?>

                <?php endif ?>
            </div>
        </div>
    </div>
</div>


<div class="modal hide fade" id="add_hospital" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Add New Hospital</h4>
    </div>

    <form method="post"  enctype="multipart/form-data">
        <div class="modal-body nopadding">
            <table class="table table-striped">
                <?php include '_hospital_form_include.php'; ?>
                <tr>
                    <td class="text-right">Hospital Logo</td>
                    <td><input type="file" name="logo" placeholder="Primary Phone Number"></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
            <button class="btn btn-primary" type="submit">Add New Hospital</button>
        </div>
    </form>
</div>

<div class="modal hide fade" id="modal_edit_hospital" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
</div>

<script src="/js/hospital.js"></script>