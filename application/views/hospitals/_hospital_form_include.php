<tr>
    <td class="text-right">Hospital Name</td>
    <td><input required type="text" name="hospital_name" placeholder="Hospital Name" value="<?= isset($hospital->hospital_name) ? $hospital->hospital_name : ''; ?>"></td>
</tr>
<tr>
    <td class="text-right">Hospital Address</td>
    <td><input type="text" name="address1" placeholder="Hospital Address" required value="<?= isset($hospital->address1) ? $hospital->address1 : ''; ?>"></td>
</tr>
<tr>
    <td class="text-right">City</td>
    <td><input type="text" name="city" placeholder="City" required value="<?= isset($hospital->city) ? $hospital->city : ''; ?>"></td>
</tr>

<tr>
    <td class="text-right">State</td>
    <td>
        <select required name="state_id" id="state_id" class="select2-me input-large">
            <option value="">Select State</option>
            <?php
            if (!empty($states)):
                $sel = '';
                foreach ($states as $state):
                    if (isset($hospital->state_id) && $hospital->state_id == $state->state_id):
                        $sel = 'selected';
                    else:
                        $sel = '';
                    endif;
                    ?>
                    <option value="<?= $state->state_id ?>" <?= $sel; ?>><?= $state->state ?></option>
                    <?php
                endforeach;
            endif;
            ?>
        </select>
    </td>
</tr>
<tr>
    <td class="text-right">Capitation Class</td>
    <td>
    <?php $capitation = get_capitation_class(); ?>
        <select required name="capitation_id" id="capitation" class="select2-me input-large">
            <option value="">Select Capitation Class</option>
            <?php
            if (!empty($capitation)):
                $sel = '';
                foreach ($capitation as $cap_id => $cap):
                    if (isset($hospital->capitation_id) && $hospital->capitation_id == $cap_id):
                        $sel = 'selected';
                    else:
                        $sel = '';
                    endif;
                    ?>
                    <option value="<?= $cap_id ?>" <?= $sel; ?>><?= $cap; ?></option>
                    <?php
                endforeach;
            endif;
            ?>
        </select>
    </td>
</tr>	
<tr>
    <td class="text-right"><strong>Contact Person</strong></td>
</tr>
<tr>
    <td class="text-right">Contact Name</td>
    <td><input required type="text" name="contact_name" placeholder="Contact Full Name" value="<?= isset($hospital->contact_name) ? $hospital->contact_name : ''; ?>"></td>
</tr>
<tr>
    <td class="text-right">Contact Phone</td>
    <td><input required type="text" name="contact_phone" placeholder="Primary Phone Number" value="<?= isset($hospital->contact_phone) ? $hospital->contact_phone : ''; ?>"></td>
</tr>
<tr>
    <td class="text-right">Contact Email</td>
    <td><input required type="text" name="contact_email" placeholder="Contact Email" value="<?= isset($hospital->contact_email) ? $hospital->contact_email : ''; ?>"></td>
</tr>