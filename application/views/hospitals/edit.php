<div class="row_fluid"> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Edit Hospital</h4>
    </div>

    <form method="post" action="<?= site_url('/hospitals/edit/'.$hospital->hospital_id)?>">
        <div class="modal-body nopadding">
            <table class="table table-striped">
                <?php include '_hospital_form_include.php'; ?>
            </table>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
            <button class="btn btn-primary" type="submit">Update Hospital</button>
        </div>
    </form>
</div>