<div class="page-header">
    <div class="pull-left">
        <h1>
            <a href="<?= site_url('/hospitals'); ?>" class="btn btn-warning btn-small">
                <i class="icons icon-chevron-left"></i> Back</a>
            Hospitals</h1>
    </div>
    <div class="clearfix"></div>
</div>


<div class="row-fluid">

    <div class="span3">
        <div class="box">
            <!--<div class="box-title" style="border:1px solid #CCCCCC;height:180px;padding:0;">
                 <div class="row-fluid" style="height:180px;overflow:hidden">
            <?php
            if ($hospital->logo != '') {
                $logo_url = "/files/hospital_logos/" . $hospital->logo;
            } else {
                $logo_url = '/files/hospital_logos/default-logo.jpg';
            }
            ?>
                    <img src="<?php echo $logo_url; ?>" />
                </div> 

            </div>-->
            <div class="_box-content">

                <div class="span12">
                    <p></p>
                    <ul class="nav nav-tabs nav-stacked left_menu">
                        <li class="active"><a class="left_menu_button"><h3><?= $hospital->hospital_name; ?></h3></a></li>
                        <li class="active"><a class="left_menu_button"><?= $hospital->address1; ?></a></li>
                        <li class="active"><a class="left_menu_button"><?= $hospital->city; ?></a></li>
                        <li class="active"><a class="left_menu_button"><?php echo get_states($hospital->state_id); ?></a></li>
                        <li class="active"><a class="left_menu_button"><strong>Capitation Class:</strong><br><?= get_capitation_class($hospital->capitation_id); ?></a></li>
                    </ul>
                    <p><strong>CONTACT PERSON</strong></p>
                    <ul class="nav nav-tabs nav-stacked left_menu">
                        <li class="active"><a class="left_menu_button"><strong><?= $hospital->contact_name; ?></strong></a></li>
                        <li class="active"><a class="left_menu_button"><?= $hospital->contact_phone; ?></a></li>
                        <li class="active"><a class="left_menu_button"><?= $hospital->contact_email; ?></a></li>
                    </ul>
                    <?php if ($this->user_auth_lib->have_perm('hospitals:edit_hospital')): ?>
                        <a href="/hospitals/edit/<?= $hospital->hospital_id; ?>" class="btn btn-primary edit_hospital">Edit</a>
                    <?php endif; ?>
                </div>

            </div>
        </div>

    </div>
    <div class="span9">
        <div class="row-fluid">

            <div class="span12">
                <div class="box box-bordered">
                    <div class="box-title">
                        <h3>
                            <i class="icon-bar-chart"></i>
                            Subscribers
                        </h3>
                        <?php if ($subscribers): ?>
                            <a href="<?= site_url('/hospitals/download_capitation_list/' . $hospital->hospital_id) ?>" class="btn btn-primary pull-right download_capitation" style="margin-right: 5px;">
                                <i class="icons icon-download-alt"></i> Download Capitation List
                            </a>  
                        <?php endif; ?>
                    </div>
                    <div class="box-content-padless">

                        <?php $this->load->view('subscribers/_subscribers'); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<div class="modal hide fade" id="modal_edit_hospital" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
</div>

<script src="/js/hospital.js"></script>