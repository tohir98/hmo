<p>&nbsp;</p>
<div class="row-fluid">
    <div class="span7">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-pushpin"></i>Quick Access</h3>
                <div class="actions">
                    <!--<a href="#" class="btn btn-mini content-remove"><i class="icon-remove"></i></a>-->
                    <a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
                </div>
            </div>
            <div class="box-content">
                <ul class="tiles">
                    <li class="blue">
                        <a href="<?= site_url('/subscribers') ?>">
                            <span>
                                <i class="icon-user"></i></span>
                            <span class='name'>Subscribers</span>
                        </a>
                    </li>

                    <li class="lime">
                        <a href="<?= site_url('/hospitals') ?>">
                            <span class="count">
                                <i class="icon-home"></i>
                                <?= count($hospitals); ?>
                            </span>
                            <span class='name'>Hospitals</span>
                        </a>
                    </li>

                    <li class="green">
                        <a href="<?= site_url('/settings/plans'); ?>">
                            <span class="count">
                                <i class="icon-cogs"></i><?= count($plans); ?>
                            </span>
                            <span class='name'>Health Plans</span>
                        </a>
                    </li>

                    <li class="orange">
                        <a href="<?= site_url('/report'); ?>"><span>
                                <i class="icon-inbox"></i></span>
                            <span class='name'>Reports</span>
                        </a>
                    </li>
<!--                    <li class="orange">
                        <a href="#"><span>
                                <i class="icon-briefcase"></i></span>
                            <span class='name'>Capitations</span>
                        </a>
                    </li>-->
                </ul>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="box box-color box-bordered green">
                    <div class="box-title">
                        <h3>
                            <i class="icon-th"></i>
                            What Would You Like to Do Today?									</h3>
                    </div>
                    <div class="box-content">
                        <div class="span3">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="max-width: 200px; max-height: 150px;"><img src="/img/logo@2x.png"></div>
                            </div>
                        </div>
                        <div class="span1">



                        </div>

                        <div class="span4">

                            <p>
                                <a class="btn" href="<?= site_url('/hospitals'); ?>">Add New Hospital</a>

                            </p>

                            <br>
                            <p>
                                <a class="btn btn-primary" href="<?= site_url('/subscribers/add'); ?>">Add New Subscriber</a>

                            </p><br><p>
                                <a class="btn" href="<?= site_url('/settings/plans'); ?>">Add New Health Plan</a>
                            </p>
                        </div>
                        <div class="span4">

                            <p>
                                <a class="btn btn-primary" href="<?= site_url('/settings/messages'); ?>">Update Default Messages</a>

                            </p>

                            <br>
                            <p>
                                <a class="btn" href="<?= site_url('/settings/hospital_class'); ?>">Setup Hospital Classification</a>

                            </p><br><p>
                                <a class="btn btn-primary" href="<?= site_url('/user/change_password'); ?>">Change My Password</a>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="span5">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-calendar"></i>My calendar</h3>
            </div>
            <div class="box-content nopadding">
                <div class="calendar"></div>
            </div>
        </div>
    </div>
</div>


