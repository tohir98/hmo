<html xmlns="http://www.w3.org/1999/xhtml">
    <?php include '_reg_header.php'; ?>
    <body class="full-padding" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #f2f2f2;">
        <!--<![endif]-->
        <div class="wrapper" style="background-color: #f2f2f2;">
            <table style="border-collapse: collapse;table-layout: fixed;color: #b8b8b8;font-family: Ubuntu,sans-serif;" align="center">
                <tbody><tr>
                        <td class="preheader__snippet" style="padding: 10px 0 5px 0;vertical-align: top;width: 280px;">

                        </td>
                        <td class="preheader__webversion" style="text-align: right;padding: 10px 0 5px 0;vertical-align: top;width: 280px;">

                        </td>
                    </tr>
                </tbody></table>

            <table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style="">
                <tbody><tr>
                        <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #60666d;font-size: 14px;line-height: 21px;font-family: sans-serif;width: 600px;">

                            <div class="image" style="font-size: 12px;font-style: normal;font-weight: 400;" align="center">
                                <a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #00afd1;" href="<?= WEBSITE; ?>" target="_blank"><img style="display: block;border: 0;max-width: 250px;" src="<?= site_url() ?>/img/hii_logo.png" alt="<?= BUSINESS_NAME_FULL; ?>" width="600"></a>
                            </div>

                            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;">
                                <div style="line-height:10px;font-size:1px">&nbsp;</div>
                            </div>

                            <div style="Margin-left: 20px;Margin-right: 20px;">
                                <h2 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #43464a;font-size: 20px;line-height: 28px;">Hi&nbsp;<?= $first_name . ' ' . $last_name; ?>,</h2>
                                <p class="size-14" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 14px;line-height: 21px;"><?= $message; ?></p>
                            </div>

                            <div style="Margin-left: 20px;Margin-right: 20px;">
                                <div style="line-height:10px;font-size:1px">&nbsp;</div>
                            </div>

                            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 24px;">
                                <p class="size-14" style="Margin-top: 0;Margin-bottom: 0;font-size: 14px;line-height: 21px;">Thanks for coming on board,<br>
                                        <strong>Health Care Int'l. Team</strong></p>
                            </div>

                        </td>
                    </tr>
                </tbody></table>

            <table class="footer" style="border-collapse: collapse;table-layout: fixed;Margin-right: auto;Margin-left: auto;border-spacing: 0;width: 560px;" align="center">
                <tbody><tr>
                        <td style="padding: 0 0 40px 0;">
                            <table class="footer__right" style="border-collapse: collapse;table-layout: auto;border-spacing: 0;" align="right">
                                <tbody><tr>
                                        <td class="footer__inner" style="padding: 0;">




                                        </td>
                                    </tr>
                                </tbody></table>
                            <table class="footer__left" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;color: #b8b8b8;font-family: Ubuntu,sans-serif;width: 380px;">
                                <tbody><tr>
                                        <td class="footer__inner" style="padding: 0;font-size: 12px;line-height: 19px;">
                                            <table class="footer__links emb-web-links" style="border-collapse: collapse;table-layout: fixed;"><tbody><tr>
                                                    </tr></tbody></table>
                                            <div style="Margin-top: 26px;">
                                                <div>Powered by <?= BUSINESS_NAME_FULL; ?><br>
                                                        For Any question, pls contact <?= INFO_EMAIL; ?></div>
                                            </div>
                                            <div class="footer__permission" style="Margin-top: 18px;">

                                            </div>
                                        </td>
                                    </tr>
                                </tbody></table>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
        <img style="visibility: hidden !important; display: block !important; height:1px !important; width:1px !important; border: 0 !important; margin: 0 !important; padding: 0 !important" src="https://o.createsend1.com/t/i-o-hdhuhly-l/o.gif" width="1" height="1" border="0" alt="">
            <script type="text/javascript" src="https://js.createsend1.com/js/jquery-1.7.2.min.js?h=C99A46590419"></script><script type="text/javascript">$(function () {
                    $('area,a').attr('target', '_blank');
                });</script>
    </body>
</html>