<?php include '_header.php'; ?>
<p> Hi <?= $first_name ?>,</p>
<br>
<p>
    This is to notify you that the sum of <b>NGN<?= number_format($amount, 2); ?></b> has been received being your premium for the period of : <strong><?= $start_date; ?> to <?= $end_date; ?></strong>
</p>
<br>
<p>
    Thanks for choosing <?= BUSINESS_NAME; ?>
</p>


<br/>

<?php include '_footer.php'; ?>