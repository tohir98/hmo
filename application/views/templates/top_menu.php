<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>

<div id="navigation">
    <div class="container-fluid">
        <a href="<?php echo $dashboard_url; ?>" id="brand"><?= BUSINESS_NAME; ?> <span style="font-size: 10px; font-weight: bold; color: #ffdf00">beta</span></a>
        <ul class='main-nav'>
            <li class=''>
                <a href="<?= site_url('/admin/dashboard') ?>">
                    <span>Dashboard</span>
                </a>
            </li>

            <li>
                <a href="<?= site_url('/hospitals'); ?>">
                    <span>Hospitals</span>
                </a>
            </li>
            <li>
                <a href="<?= site_url('/subscribers'); ?>">
                    <span>Subcribers</span>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span>Settings</span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <?php if ($this->user_auth_lib->have_perm('health_plan:view')): ?>
                    <li>
                        <a href="<?= site_url('settings/hospital_class') ?>">Hospital Class</a>
                    </li>
                    <?php endif; ?>
                    <?php if ($this->user_auth_lib->have_perm('health_plan:view')): ?>
                    <li>
                        <a href="<?= site_url('settings/benefit_categories') ?>">Benefit Categories</a>
                    </li>
                    <?php endif; ?>
                    <?php if ($this->user_auth_lib->have_perm('health_plan:view')): ?>
                    <li>
                        <a href="<?= site_url('settings/all_benefits') ?>">Benefit</a>
                    </li>
                    <?php endif; ?>
                    <?php if ($this->user_auth_lib->have_perm('health_plan:view')): ?>
                    <li>
                        <a href="<?= site_url('settings/plans') ?>">Plans</a>
                    </li>
                    <?php endif; ?>
                    <?php if ($this->user_auth_lib->have_perm('setup:messages')): ?>
                    <li>
                        <a href="<?= site_url('settings/messages') ?>">Messages</a>
                    </li>
                    <?php endif; ?>
                </ul>
            </li>
            <li><a href="<?= site_url('/report') ?>">Reports &amp; Analytics</a></li>

            <li>
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span><?= ucfirst($display_name); ?></span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li><a href="<?= site_url('/user/change_password') ?>">Change Password</a></li>
                    <?php if ($this->user_auth_lib->is_super_admin()): ?>
                        <li><a href="<?= site_url('/access_control') ?>">Access Control</a></li>
                        <li><a href="<?= site_url('/admin/user_types') ?>">User Types</a></li>
                        <li><a href="<?= site_url('/admin/users') ?>">User Mgt</a></li>
                    <?php endif; ?>

                    <li><a href="<?php echo $logout_url; ?>">Sign out</a></li>
                </ul>
            </li>

        </ul>
    </div>
</div>
