<div class="row_fluid"> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Edit Plan Benefit</h4>
    </div>

    <form name="frmadd" id="frmadd" method="post" action="<?= site_url('/settings/plans/edit_benefit/'. $benefit->health_plan_benefit_id); ?>" class="form-horizontal form-bordered">
        <?php include '_plan_benefit_form.php'; ?>
        <div class="modal-footer" id="footer_modal">>
            <button data-dismiss="modal" class="btn btn-warning" aria-hidden="true">Cancel</button>
            <input type="submit" class="btn btn-primary" value="Update Benefit" />
        </div>
    </form>
</div>