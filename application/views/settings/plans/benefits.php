<?= show_notification(); ?>
<div class="page-header">
    <div class="pull-left">
        <h1>Settings</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>


<div class="box">
    <div class="box-content nopadding">
        <?php include APPPATH . 'views/settings/_tab.php'; ?>

        <div class="tab-content"> 
            <div class="tab-pane active" id="userList">
                <a class="btn btn-warning" href="<?= site_url('/settings/plans'); ?>">
                    <i class="icons icon-chevron-left"></i> Back
                </a>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="box box-bordered box-color">
                            <div class="box-title">
                                <h3><?= $plan->health_plan; ?> Health Plan</h3>
                            </div>
                            <div class="box-content-padless">
                                <table class="table table-striped">
                                    <tr>
                                        <th>Premium</th>
                                        <th>NGN <?= number_format($plan->premium, 2); ?></th>
                                    </tr>
                                    <tr>
                                        <th>No. of Dependants</th>
                                        <th><?= $plan->max_dependant; ?></th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="span8">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-bar-chart"></i>
                                    Benefits
                                </h3>
                                <a href="#new_benefit" data-toggle="modal" class="btn btn-success pull-right" style="margin-right: 5px"> 
                                    <i class="icons icon-plus-sign"></i> Add Benefit</a>
                            </div>
                            <div class="box-content-padless">
                                <?php if (!empty($benefits)): ?>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Benefit</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sn = 0;
                                            foreach ($benefits as $benefit):
                                                ?>
                                                <tr>
                                                    <td><?= ++$sn; ?></td>
                                                    <td><?= ucfirst($benefit->benefit) ?></td>
                                                    <td><?= $benefit->benefit_desc; ?></td>
                                                    <td>
                                                        <a class="edit_benefit" href="<?= site_url('/settings/plans/edit_benefit/'. $benefit->health_plan_benefit_id); ?>">
                                                            <i class="icons icon-edit"></i>
                                                            edit</a>|
                                                        <a href="<?= site_url('/settings/plans/remove_benefit/' . $benefit->health_plan_benefit_id); ?>" class="delete_plan" title="Remove this benefit?">
                                                            <i class="icons icon-trash"></i>
                                                            delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data("No benefit has been added for {$plan->health_plan} plan. ");
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php include '_new_benefit.php'; ?>

<div class="modal hide fade" id="modal_edit_benefit" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
</div>

<script>
    $(function () {
        $('body').delegate('.delete_plan', 'click', function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to remove this benefit ?';
            CTM.doConfirm({
                title: 'Confirm',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
    
     $('body').delegate('.edit_benefit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_benefit').modal('show');
        $('#modal_edit_benefit').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_benefit').html('');
            $('#modal_edit_benefit').html(html);
            $('#modal_edit_benefit').modal('show').fadeIn();
        });
        return false;
    });
</script>
