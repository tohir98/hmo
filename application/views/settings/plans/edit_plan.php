<div class="row_fluid"> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Edit Health Plan</h4>
    </div>

    <form name="frmUpdate" id="frmUpdate" method="post" action="<?= site_url('/settings/plans/edit_plan/' . $plan->health_plan_id); ?>" class="form-horizontal form-bordered">
        <?php include '_plan_form.php'; ?>
        <div class="modal-footer" id="footer_modal">
            <button data-dismiss="modal" class="btn btn-warning" aria-hidden="true"> Cancel </button>
            <input type="submit" class="btn btn-primary" value="Update Plan" />
        </div>
    </form>
</div>