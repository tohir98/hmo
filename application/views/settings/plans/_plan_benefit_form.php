<div class="modal-body nopadding">
    <div class="control-group">
        <label for="health_plan" class="control-label">Benefit</label>
        <div class="controls">
            <select required name="benefit_id" id="benefit_id" class="select2-me input-large">
                <option value="">Select Benefit</option>
                <?php
                if (!empty($n_benefits)):
                    $sel = '';
                    foreach ($n_benefits as $n_benefit):
                        if (isset($n_benefit->benefit_id) && $n_benefit->benefit_id == $n_benefit->benefit_id):
                            $sel = 'selected';
                        else:
                            $sel = '';
                        endif;
                        ?>
                        <option value="<?= $n_benefit->benefit_id ?>" <?= $sel; ?>><?= $n_benefit->benefit; ?></option>
                        <?php
                    endforeach;
                endif;
                ?>
            </select>
        </div>
    </div>
</div>