<?= show_notification(); ?>

<div class="page-header">
    <div class="pull-left">
        <h1>Settings</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>


<div class="box">
    <div class="box-content nopadding">
        <?php include APPPATH . 'views/settings/_tab.php'; ?>

        <div class="tab-content"> 
            <div class="tab-pane active" id="userList">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-bar-chart"></i>
                                    Health Plans
                                </h3>
                                <?php if ($this->user_auth_lib->have_perm('health_plan:add_plan')): ?>
                                <a href="#new_plan" data-toggle="modal" class="btn btn-success pull-right" style="margin-right: 5px"> 
                                    <i class="icons icon-plus-sign"></i> New Plan</a>
                                    <?php endif; ?>
                            </div>
                            <div class="box-content-padless">
                                <?php if (!empty($plans)): ?>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Health Plan</th>
                                                <th>No. of Dependants</th>
                                                <th>Premium</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sn = 0;
                                            foreach ($plans as $plan):
                                                ?>
                                                <tr>
                                                    <td><?= ++$sn; ?></td>
                                                    <td>
                                                        <a href="<?= site_url('/settings/plans/plan_benefits/' . $plan->health_plan_id); ?>" title="View plan benefits"><?= ucfirst($plan->health_plan) ?>
                                                        </a>
                                                    </td>
                                                    <td><?= $plan->max_dependant; ?></td>
                                                    <td>NGN <?= number_format($plan->premium, 2); ?></td>
                                                    <td>
                                                        <?php if ($this->user_auth_lib->have_perm('health_plan:edit_plan')): ?>
                                                        <a class="edit_plan" href="<?= site_url('/settings/plans/edit_plan/' . $plan->health_plan_id); ?>">
                                                            <i class="icons icon-edit"></i>
                                                            edit</a>| <?php endif; ?>
                                                        <?php if ($this->user_auth_lib->have_perm('health_plan:delete_plan')): ?>
                                                        <a href="<?= site_url('/settings/plans/delete_plan/' . $plan->health_plan_id); ?>" class="delete_plan" title="Delete this plan?">
                                                            <i class="icons icon-trash"></i>
                                                            delete
                                                        </a> |<?php endif; ?>
                                                        <?php if ($this->user_auth_lib->have_perm('health_plan:plan_benefits')): ?>
                                                        <a href="<?= site_url('/settings/plans/plan_benefits/' . $plan->health_plan_id); ?>" title="View plan benefits">
                                                            <i class="icons icon-bitbucket"></i>
                                                            Benefits
                                                        </a><?php endif; ?>
                                                    </td>
                                                </tr>
    <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data('No health plan has been added');
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php include '_new_plan.php'; ?>

<div class="modal hide fade" id="modal_edit_plan" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
</div>

<script>
    $(function () {
        $('body').delegate('.delete_plan', 'click', function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this plan ?';
            CTM.doConfirm({
                title: 'Confirm',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit_plan', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_plan').modal('show');
        $('#modal_edit_plan').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_plan').html('');
            $('#modal_edit_plan').html(html);
            $('#modal_edit_plan').modal('show').fadeIn();
        });
        return false;
    });
</script>
