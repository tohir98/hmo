<div class="modal-body nopadding">
    <div class="control-group">
        <label for="health_plan" class="control-label">Health Plan</label>
        <div class="controls">
            <input required type="text" placeholder="" class='input'  value="<?= isset($plan->health_plan) ? $plan->health_plan : ''; ?>" id="health_plan" name="health_plan">
        </div>
    </div>
    <div class="control-group">
        <label for="description" class="control-label">Description</label>
        <div class="controls">
            <input  type="text" placeholder="" class='input'  value="<?= isset($plan->description) ? $plan->description : ''; ?>" id="description" name="description">
        </div>
    </div>
    <div class="control-group">
        <label for="premium" class="control-label">Premium</label>
        <div class="controls">
            <input required type="text" placeholder="7500" value="<?= isset($plan->premium) ? $plan->premium : ''; ?>" class='input' id="premium" name="premium">
        </div>
    </div>
    <div class="control-group">
        <label for="premium" class="control-label">No. of Dependants</label>
        <div class="controls">
            <input required type="text" placeholder="6" value="<?= isset($plan->max_dependant) ? $plan->max_dependant : '6'; ?>" class='input' id="max_dependant" name="max_dependant" />
        </div>
    </div>
</div>