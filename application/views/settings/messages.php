<?= show_notification(); ?>

<div class="page-header">
    <div class="pull-left">
        <h1>Settings</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>


<div class="box">
    <div class="box-content nopadding">
        <?php include APPPATH . 'views/settings/_tab.php'; ?>

        <div class="tab-content"> 
            <div class="tab-pane active" id="userList">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-bar-chart"></i>
                                    Messages
                                </h3>
                                <?php if ($this->user_auth_lib->have_perm('setup:edit_message')) : ?>
                                    <a href="#edit_messages" data-toggle="modal" class="btn btn-primary pull-right" style="margin-right: 5px;">
                                        Edit
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div class="box-content-padless">
                                <table class="table table-striped table-hover">

                                    <tbody>
                                        <tr>
                                            <td>Sign Up Email</td>
                                            <td>
                                                <p class="alert alert-info"><?= $settings->sign_up_email; ?></p>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>Sign Up SMS</td>
                                            <td>
                                                <p class="alert alert-info"><?= $settings->sign_up_sms; ?></p>
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal hide fade" id="edit_messages" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
    <div class="row_fluid"> 
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Edit Message</h4>
        </div>

        <form name="frmadd" id="frmadd" method="post" action="" class="form-horizontal">
            <div class="modal-body">
                <div class="control-group">
                    <label class="control-label">Email</label>
                    <div class="controls">
                        <textarea name="sign_up_email" id="sign_up_email"  style="width: 90%"><?= $settings->sign_up_email; ?></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">SMS</label>
                    <div class="controls">
                        <textarea name="sign_up_sms" id="sign_up_sms" style="width: 90%"><?= $settings->sign_up_sms; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="footer_modal">
                <button data-dismiss="modal" class="btn btn-warning" aria-hidden="true">Cancel</button>
                <input type="submit" class="btn btn-primary" value="Update Message" />
            </div>
        </form>
    </div>
</div>