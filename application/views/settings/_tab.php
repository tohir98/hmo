<?php $path = $this->input->server('REQUEST_URI'); ?>
<ul class="nav nav-tabs" id="myTab">
    <li class="<?= strstr($path, 'settings/plans') ? 'active' : '' ?>">
        <a href="<?= site_url('/settings/plans') ?>">Plans</a>
    </li>
    <li class="<?= strstr($path, 'settings/benefit_categories') ? 'active' : '' ?>">
        <a href="<?= site_url('/settings/benefit_categories') ?>">Benefit Categories</a>
    </li>
    <li class="<?= strstr($path, 'settings/all_benefits') || strstr($path, 'settings/benefits') ? 'active' : '' ?>">
        <a href="<?= site_url('/settings/all_benefits') ?>">Benefits</a>
    </li>
    <li class="<?= strstr($path, 'settings/messages') ? 'active' : '' ?>">
        <a href="<?= site_url('/settings/messages') ?>">Messages</a>
    </li>
    <li class="<?= strstr($path, 'settings/hospital_class') ? 'active' : '' ?>">
        <a href="<?= site_url('/settings/hospital_class') ?>">Hospital Class</a>
    </li>
</ul>
