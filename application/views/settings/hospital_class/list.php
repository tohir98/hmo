<?= show_notification(); ?>

<div class="page-header">
    <div class="pull-left">
        <h1>Settings</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>


<div class="box">
    <div class="box-content nopadding">
        <?php include APPPATH . 'views/settings/_tab.php'; ?>

        <div class="tab-pane active" id="userList">
            <div class="row-fluid">
                <div class="span12">
                    <div class="box box-bordered">
                        <div class="box-title">
                            <h3>Hospital Class</h3>
                            <?php if ($this->user_auth_lib->have_perm('setup:edit_message')) : ?>
                                <a href="#add_hospital" data-toggle="modal" class="btn btn-primary pull-right" style="margin-right: 5px;">
                                    Add Hospital Class
                                </a>
                            <?php endif; ?>
                        </div>
                        <div class="box-content-padless">
                        <?php if (empty($hospitals)): ?>
                        	<?php echo show_no_data('Hi, there are no hospital class created yet.'); ?>
                        <?php else: ?>
                        	<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>Name</th>
										<th>Decription</th>
										<th>Capitation/Month</th>
										<th>Date Created</th>
										<th>Action</th>
									</tr>
								</thead>
                                <tbody>
                                <?php foreach ($hospitals as $h): ?>
                                	<tr>
                                        <td><?=$h->class;?></td>
                                        <td><?=$h->description;?></td>
                                        <td><?=number_format($h->capitation, 2);?></td>
                                        <td><?=date('d-M-Y', strtotime($h->date_created));?></td>
                                        <td>
											<div class="btn-group">
	                                            <a href="#" data-toggle="dropdown" class="btn btn-primary pull-left dropdown-toggle">Action <span class="caret"></span></a>
	                                            <ul class="dropdown-menu">
	                                                <li><a class="edit_hospital" href="#" data-id="<?= $h->id;?>" data-name="<?= $h->class;?>" data-description="<?= $h->description;?>" data-capitation="<?= $h->capitation;?>">Edit</a></li>
	                                                <li><a href="#" class="delete_hospital" data-id="<?= $h->id; ?>" data-name="<?=$h->class;?>">Delete</a></li>
	                                            </ul>
	                                        </div>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



<div class="modal hide fade" id="delete_hospital" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
    <div class="row_fluid"> 
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Delete Hospital Class</h4>
        </div>

        <form name="frmadd" id="frmadd" method="post" action="" class="form-horizontal">
            <div class="modal-body">
                <p>Are you sure you want to delete <strong><span class="classname"></span></strong> hospital class? Make sure there are no hospitals assigned this class.</p>
				<input type="hidden" name="id_delete" value="">
            </div>
            <div class="modal-footer" id="footer_modal">
                <button data-dismiss="modal" class="btn btn-warning" aria-hidden="true">Cancel</button>
                <input type="submit" class="btn btn-primary" value="Delete Hospital Class" />
            </div>
        </form>
    </div>
</div>


<div class="modal hide fade" id="add_hospital" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
    <div class="row_fluid"> 
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Add Hospital Class</h4>
        </div>

        <form name="frm" method="post" action="" class="form-horizontal">
            <div class="modal-body">
                <div class="control-group">
                    <label class="control-label">Class Name</label>
                    <div class="controls">
                        <input type="text" name="name">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Description</label>
                    <div class="controls">
                        <textarea name="description" style="width: 90%"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Capitation/Month</label>
                    <div class="controls">
                        <input type="text" name="capitation">
                    </div>
                </div>
				<input type="hidden" name="id" value="0">
            </div>
            <div class="modal-footer" id="footer_modal">
                <button data-dismiss="modal" class="btn btn-warning" aria-hidden="true">Cancel</button>
                <input type="submit" class="btn btn-primary" value="Save Hospital Class" />
            </div>
        </form>
    </div>
</div>


<script type="text/javascript">
	
	$('body').on('click', '.edit_hospital', function(){
		var id = $(this).data('id');
		var name = $(this).data('name');
		var description = $(this).data('description');
		var capitation = $(this).data('capitation');

		$('input[name="name"]').val(name);
		$('textarea[name="description"]').val(description);
		$('input[name="capitation"]').val(capitation);
		$('input[name="id"]').val(id);

		$('#add_hospital').modal('show');
	});

	$('body').on('click', '.delete_hospital', function(){
		var id = $(this).data('id');
		var name = $(this).data('name');

		$('.classname').html(name);
		$('input[name="id_delete"]').val(id);

		$('#delete_hospital').modal('show');
	});

</script>