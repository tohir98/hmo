<div class="modal-body nopadding">
    <div class="control-group">
        <label for="benefit" class="control-label">Benefit</label>
        <div class="controls">
            <input required type="text" placeholder="" class='input-xlarge'  value="<?= isset($e_benefit->benefit) ? $e_benefit->benefit : ''; ?>" id="benefit" name="benefit">
        </div>
    </div>
    <div class="control-group">
        <label for="description" class="control-label">Description</label>
        <div class="controls">
            <input  type="text" placeholder="" class='input-xlarge'  value="<?= isset($e_benefit->benefit_desc) ? $e_benefit->benefit_desc : ''; ?>" id="benefit_desc" name="benefit_desc">
        </div>
    </div>
    <div class="control-group">
        <label for="description" class="control-label">Category</label>
        <div class="controls">
            <select required name="benefit_category_id" id="benefit_category_id" class="select2-me input-large">
            <option value="">Select Category</option>
            <?php
            if (!empty($categories)):
                $sel = '';
                foreach ($categories as $category):
                    if (isset($category->benefit_category_id) && $category->benefit_category_id == $e_benefit->benefit_category_id):
                        $sel = 'selected';
                    else:
                        $sel = '';
                    endif;
                    ?>
                    <option value="<?= $category->benefit_category_id ?>" <?= $sel; ?>><?= $category->category_name; ?></option>
                    <?php
                endforeach;
            endif;
            ?>
        </select>
        </div>
    </div>
</div>