<div class="modal hide fade" id="new_benefit" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
    <div class="row_fluid"> 
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Add Benefit</h4>
        </div>

        <form name="frmadd" id="frmadd" method="post" action="" class="form-horizontal form-bordered">
            <?php include '_benefit_form.php'; ?>
            <div class="modal-footer" id="footer_modal">
                <button data-dismiss="modal" class="btn btn-warning" aria-hidden="true">Cancel</button>
                <input type="submit" class="btn btn-primary" value="Save Benefit" />
            </div>
        </form>
    </div>
</div>