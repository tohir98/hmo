<?= show_notification(); ?>

<div class="page-header">
    <div class="pull-left">
        <h1>Settings</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>


<div class="box">
    <div class="box-content nopadding">
        <?php include APPPATH . 'views/settings/_tab.php'; ?>
        
        <select id="categorylist" class="select2-me input-large">
            <option value="">All Categories</option>
            <?php
            if (!empty($categories)):
                $sel = '';
                foreach ($categories as $category):
                    if ($category->benefit_category_id == $this->uri->segment(3)):
                        $sel = 'selected';
                    else:
                        $sel = '';
                    endif;
                    ?>
                    <option value="<?= $category->benefit_category_id ?>" <?= $sel; ?>><?= $category->category_name; ?></option>
                    <?php
                endforeach;
            endif;
            ?>
        </select>

        <div class="tab-content"> 
            <div class="tab-pane active" id="userList">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-bar-chart"></i>
                                    Health Benefits
                                </h3>
                                <?php if ($this->user_auth_lib->have_perm('health_plan:add_plan')): ?>
                                    <a href="#new_benefit" data-toggle="modal" class="btn btn-success pull-right" style="margin-right: 5px"> 
                                        <i class="icons icon-plus-sign"></i> Add Benefit</a>
                                <?php endif; ?>
                            </div>
                            <div class="box-content-padless">
                                <?php if (!empty($benefits)): ?>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Benefit</th>
                                                <th>Benefit Description</th>
                                                <th>Capitated</th>
                                                <th>Category</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sn = 0;
                                            foreach ($benefits as $benefit):
                                                ?>
                                                <tr>
                                                    <td><?= ++$sn; ?></td>
                                                    <td>
                                                        <?= $benefit->benefit; ?>
                                                    </td>
                                                    <td><?= $benefit->benefit_desc; ?></td>
                                                    <td><?= $benefit->capitated ? 'Yes' : 'No'; ?></td>
                                                    <td><?= $benefit->category_name; ?></td>
                                                    <td>
                                                        <?php if ($this->user_auth_lib->have_perm('health_plan:edit_plan')): ?>
                                                            <a class="edit_category" href="<?= site_url('/settings/plans/edit_all_benefit/' . $benefit->benefit_id); ?>">
                                                                <i class="icons icon-edit"></i>
                                                                edit</a>| <?php endif; ?>
                                                        <?php if ($this->user_auth_lib->have_perm('health_plan:delete_plan')): ?>
                                                            <a href="<?= site_url('/settings/plans/delete_benefit/' . $benefit->benefit_id); ?>" class="delete_benefit" title="Delete this benefit?">
                                                                <i class="icons icon-trash"></i>
                                                                delete
                                                            </a><?php endif; ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data('No benefit has been added');
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php include '_new_benefit.php'; ?>

<div class="modal hide fade" id="modal_edit_benefit" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
</div>

<script>
    $(function () {
        $('body').delegate('.delete_benefit', 'click', function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this benefit ?';
            CTM.doConfirm({
                title: 'Confirm',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit_category', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_benefit').modal('show');
        $('#modal_edit_benefit').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_benefit').html('');
            $('#modal_edit_benefit').html(html);
            $('#modal_edit_benefit').modal('show').fadeIn();
        });
        return false;
    });
    
    $('#categorylist').change(function () {
        var category_ = $(this).val();
        if (category_.trim() === '') {
            window.location.href = '<?= site_url('/settings/all_benefits'); ?>';
        }else{
            window.location.href = '<?= site_url('/settings/benefits'); ?>/' + category_;
        }
    });
</script>
