<div class="row_fluid"> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Edit Benefit Category</h4>
    </div>

    <form name="frmUpdate" id="frmUpdate" method="post" action="<?= site_url('/settings/plans/edit_benefit_category/' . $b_category->benefit_category_id); ?>" class="form-horizontal form-bordered">
        <?php include '_benefit_category_form.php'; ?>
        <div class="modal-footer" id="footer_modal">
            <button data-dismiss="modal" class="btn btn-warning" aria-hidden="true"> Cancel </button>
            <input type="submit" class="btn btn-primary" value="Update Category" />
        </div>
    </form>
</div>