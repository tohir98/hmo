<?= show_notification(); ?>

<div class="page-header">
    <div class="pull-left">
        <h1>Settings</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>


<div class="box">
    <div class="box-content nopadding">
        <?php include APPPATH . 'views/settings/_tab.php'; ?>

        <div class="tab-content"> 
            <div class="tab-pane active" id="userList">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-bar-chart"></i>
                                    Benefit Category
                                </h3>
                                <?php if ($this->user_auth_lib->have_perm('health_plan:add_plan')): ?>
                                    <a href="#new_benefit_category" data-toggle="modal" class="btn btn-success pull-right" style="margin-right: 5px"> 
                                        <i class="icons icon-plus-sign"></i> Add Benefit Category</a>
                                <?php endif; ?>
                            </div>
                            <div class="box-content-padless">
                                <?php if (!empty($benefit_categories)): ?>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Category Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sn = 0;
                                            foreach ($benefit_categories as $benefit_category):
                                                ?>
                                                <tr>
                                                    <td><?= ++$sn; ?></td>
                                                    <td>
                                                        <?= $benefit_category->category_name; ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($this->user_auth_lib->have_perm('health_plan:edit_plan')): ?>
                                                        <a class="edit_category" href="<?= site_url('/settings/plans/edit_benefit_category/' . $benefit_category->benefit_category_id); ?>">
                                                            <i class="icons icon-edit"></i>
                                                            edit</a>| <?php endif; ?>
                                                        <?php if ($this->user_auth_lib->have_perm('health_plan:delete_plan')): ?>
                                                        <a href="<?= site_url('/settings/plans/delete_benefit_category/' . $benefit_category->benefit_category_id); ?>" class="delete_category" title="Delete this category?">
                                                            <i class="icons icon-trash"></i>
                                                            delete
                                                        </a> |<?php endif; ?>
                                                        <?php if ($this->user_auth_lib->have_perm('health_plan:plan_benefits')): ?>
                                                        <a href="<?= site_url('settings/benefits/' . $benefit_category->benefit_category_id); ?>" title="View benefits category">
                                                            <i class="icons icon-bitbucket"></i>
                                                            View benefits in category 
                                                        </a><?php endif; ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo show_no_data('No benefit category has been added');
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php include '_new_benefit_category.php'; ?>

<div class="modal hide fade" id="modal_edit_category" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
</div>

<script>
    $(function () {
        $('body').delegate('.delete_category', 'click', function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this category ?';
            CTM.doConfirm({
                title: 'Confirm',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit_category', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_category').modal('show');
        $('#modal_edit_category').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_category').html('');
            $('#modal_edit_category').html(html);
            $('#modal_edit_category').modal('show').fadeIn();
        });
        return false;
    });
</script>
