<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property User_auth_lib $user_auth_lib Description
 * @property Subscriber_model $s_model Description
 * @property Basic_model $basic_model Description
 * @property CI_Loader $load Description
 * @property Settings_model $settings_model Description
 */
class Subscribers_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
//Check user login
        $this->load->library(['smsAdapter', 'mailer']);
        $this->user_auth_lib->check_login();

        $this->load->model('subscriber/Subscriber_model', 's_model');
        $this->load->model('settings/settings_model');
        $this->load->model('basic_model');
    }

// List all your items
    public function index() {
        $this->user_auth_lib->check_perm('subscriber:view_subscriber');
        $data['hospitals'] = $this->general->get_count('hospitals');
        $data['subscribers'] = $this->s_model->getSubscriberId();
        $this->user_nav_lib->run_page('subscribers/list', $data, BUSINESS_NAME);
    }

// Add a new item
    public function details($id) {
        if (!is_numeric($id)) {
            notify('error', 'Error! Invalid parameter provided.', 'subscribers');
        }

        $subscriber = $this->general->get_one('subscribers', ['subscriber_id' => $id, 'subscriber_type_id' => SUBSCRIBER]);

        if (empty($subscriber)) {
            notify('error', 'Error! Invalid parameter provided.', 'subscribers');
        }

//update plan enrollment
        $this->s_model->add_health_plan();

//edit plan enrollment
        $this->s_model->edit_health_plan();

//delete plan enrollment
        $this->s_model->delete_health_plan();

//add premium
        $this->s_model->add_premium();

//edit premium payment
        $this->s_model->edit_premium();

//delete premium payment
        $this->s_model->delete_premium();

        $data['subscriber'] = $subscriber;
        $data['subscribers'] = $this->general->get_many('subscribers', ['subscriber_type_id' => SUBSCRIBER]);
        $data['hospitals'] = $this->general->all('hospitals');
        $data['plans'] = $this->general->plans();
        $data['enrollments'] = get_subscriber_type();
        $data['myplan'] = $this->s_model->my_plan($subscriber->subscriber_id);
        $data['dependants'] = $this->general->get_many('subscribers', ['subscriber_parent_id' => $subscriber->subscriber_id]);
        $data['enrollment'] = $this->general->get_one('enrollments', ['subscriber_id' => $subscriber->subscriber_id, 'is_deleted' => 0]);
        $data['premiums'] = $this->general->get_many('premium_payments', ['subscriber_id' => $subscriber->subscriber_id, 'is_deleted' => 0]);
        $this->user_nav_lib->run_page('subscribers/details', $data, BUSINESS_NAME);
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     * */
    public function add() {
        $this->user_auth_lib->check_perm('subscriber:create_subscriber');
        $this->s_model->save_subscriber();
        $this->user_nav_lib->run_page('subscribers/add_subscriber', '', BUSINESS_NAME);
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     * */
    public function edit($id_subscriber) {
        if (!is_numeric($id_subscriber)) {
            notify('error', 'Error! Invalid parameter provided.', 'subscribers');
        }

        $this->s_model->save_subscriber($id_subscriber);

        $data['subscriber'] = $this->general->get_one('subscribers', ['subscriber_id' => $id_subscriber]);
        $this->user_nav_lib->run_page('subscribers/edit_subscriber', $data, BUSINESS_NAME);
    }
    
    public function edit_organization($id) {
        $this->user_auth_lib->check_login();
        
        
        $data = [
            'organization' => $this->basic_model->fetch_all_records(TBL_ORG, ['organization_id' => $id])[0],
        ];
        $this->user_nav_lib->run_page('subscribers/edit_organization', $data, BUSINESS_NAME);
    }

//Update one item
    public function add_dependant($id_subscriber) {
        if (!is_numeric($id_subscriber)) {
            notify('error', 'Error! Invalid parameter provided.', 'subscribers');
        }

        if (request_is_post()) {
            $this->s_model->save_subscriber();
        }

        $data['subscriber_id'] = $id_subscriber;
        $this->user_nav_lib->run_page('subscribers/add_dependant', $data, BUSINESS_NAME);
    }

//Delete one item
    public function dependant($dependant_id) {
        if (!is_numeric($dependant_id)) {
            notify('error', 'Error! Invalid parameter provided.', 'subscribers');
        }

        $subscriber = $this->general->get_one('subscribers', ['subscriber_id' => $dependant_id, 'subscriber_type_id' => DEPENDANTS]);

        if (empty($subscriber)) {
            notify('error', 'Error! Invalid parameter provided.', 'subscribers');
        }

        $provider = $this->general->get_one('subscribers', ['subscriber_id' => $subscriber->subscriber_parent_id]);
        $data['subscriber'] = $subscriber;
        $data['plan'] = $this->s_model->my_plan($provider->subscriber_id);
        $data['provider'] = $provider;

        $this->user_nav_lib->run_page('subscribers/dependant', $data, BUSINESS_NAME);
    }

    public function organizations() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->settings_model->saveOrganization(request_post_data())) {
                notify('success', "Organization added successfully");
            } else {
                notify('error', "Unable to add organization at the moment, pls try again later");
            }
            redirect(site_url("/subscribers/organizations"));
        }

        $pageData = [
            'organizations' => $this->s_model->fetchOrgs(),
            'states' => $this->s_model->fetchStates()
        ];
        $this->user_nav_lib->run_page('subscribers/organizations', $pageData, 'Organizations | ' . BUSINESS_NAME);
    }

    public function test_email() {
        $this->load->view('email_templates/account_signup');
    }

}

/* End of file Subscribers_controller.php */
/* Location: ./application/controllers/subscribers/Subscribers_controller.php */
