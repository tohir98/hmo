<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property General_model $general Description
 * @property Subscriber_model $sub_model Description
 * @property Hospital_model $h_model Description
 * @property Excel $excel Description
 */
class Hospital_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //Check user login
        $this->user_auth_lib->check_login();
        $this->load->model('hospital/Hospital_model', 'h_model');
        $this->load->model('subscriber/Subscriber_model', 'sub_model');
        $this->load->model('basic_model');
    }

    // List all your items
    public function index() {

        //add hospital
        $this->h_model->add_hospital();

        $data['hospitals'] = $this->h_model->fetchHospitals();
        $data['states'] = $this->general->all('states');
        $this->user_nav_lib->run_page('hospitals/list', $data, BUSINESS_NAME);
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     * */
    public function details($id = NULL) {
        if (!$id) {
            notify('error', 'Error! Invalid parameter provided.', '/hospitals');
        }

        $hospital = $this->general->get_one('hospitals', ['hospital_id' => $id]);

        if (empty($hospital)) {
            notify('error', 'Error! Invalid parameter provided.', '/hospitals');
        }

        $data['hospital'] = $hospital;
        $data['subscribers'] = $this->sub_model->getSubscriberByHospitalId($id);
        $this->user_nav_lib->run_page('hospitals/details', $data, BUSINESS_NAME);
    }

    // Add a new item
    public function add() {
        $data[] = "";
        $this->user_nav_lib->run_page('hospitals/add', $data, BUSINESS_NAME);
    }

    //Update one item
    public function update($id = NULL) {
        $data[] = "";
        $this->user_nav_lib->run_page('hospitals/update', $data, BUSINESS_NAME);
    }

    //Delete one item
    public function delete($id = NULL) {
        
    }

    public function edit($hospital_id) {
        $this->user_auth_lib->check_perm('setup:edit_hospital');
        if (request_is_post()) {
            if ($this->basic_model->update(TBL_HOSPITAL, request_post_data(), ['hospital_id' => $hospital_id])) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Unable to update health plan at moment, Please try again later");
            }
            redirect(site_url('/hospitals'));
        }

        $data = [
            'states' => $this->general->all('states'),
            'hospital' => $this->basic_model->fetch_all_records(TBL_HOSPITAL, ['hospital_id' => $hospital_id])[0]
        ];

        $this->load->view('hospitals/edit', $data);
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function download_capitation_list($hospital_id)
    {
        
        $status = $this->input->post('status');
        $hospital = $this->general->get_one('hospitals', ['hospital_id'=>$hospital_id]);
        $subs = $this->h_model->get_hospital_subscriber($hospital_id);
        $name = $hospital->hospital_name.time();

        $this->load->library('excel');
        $this->excel->download_capitation_list($subs, $hospital->hospital_name, $name);

    }

}

/* End of file Hospital.php */
/* Location: ./application/controllers/hospital/Hospital.php */
