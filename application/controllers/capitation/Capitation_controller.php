<?php

/**
 * Description of Capitation_controller
 *
 * @author tohir
 * @property User_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 */
class Capitation_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('basic_model');
        $this->load->library(['User_nav_lib']);

        $this->user_auth_lib->check_login();
    }

    public function index() {
        
    }

}
