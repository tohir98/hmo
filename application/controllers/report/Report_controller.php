<?php

/**
 * Description of Report_controller
 *
 * @author tohir
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 */
class Report_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();


        //Check user login
        $this->user_auth_lib->check_login();

        $this->load->model('subscriber/Subscriber_model', 's_model');
        $this->load->model('settings/settings_model');
    }

    public function index() {
        $pageData = [];

        $this->user_nav_lib->run_page('report/report', $pageData, 'Analytic & Reports | ' . BUSINESS_NAME);
    }
    
    public function analytics() {
         $pageData = [];

        $this->user_nav_lib->run_page('report/analytics', $pageData, 'Analytic | ' . BUSINESS_NAME);
    }

}
