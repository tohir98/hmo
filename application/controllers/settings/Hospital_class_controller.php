<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hospital_class_controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->user_auth_lib->check_login();
		$this->load->model('settings/settings_model', 'setting');
	}

	public function index()
	{

		$this->setting->add_hospital_class();
		$this->setting->delete_hospital_class();

		$data['hospitals'] = $this->general->all('hospital_class');
        $this->user_nav_lib->run_page('settings/hospital_class/list', $data, BUSINESS_NAME);
	}

}

/* End of file Hospital_class_controller.php */
/* Location: ./application/controllers/settings/Hospital_class_controller.php */