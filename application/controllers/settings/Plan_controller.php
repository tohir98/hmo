<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Plan_controller
 *
 * @author tohir
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 * @property Basic_model $basic_model Description
 * @property Settings_model $settings_model Description
 * @property Benefit_model $benefit_model Description
 * @property CI_Input $input Description
 * @property CI_Loader $load Description
 */
class Plan_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('user/User_model', 'u_model');
        $this->load->model('settings/settings_model');
        $this->load->model('settings/benefit_model');
        $this->load->model('basic_model');
        $this->load->library(['User_nav_lib', 'mailer']);
    }

    public function all_benefits($id=null) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->settings_model->saveBenefit(request_post_data())) {
                notify('success', "Benefit added successfully");
            } else {
                notify('error', "Unable to add benefit at moment, pls try again later");
            }

            redirect(site_url('settings/all_benefits'));
        }

        $pageData = [
            'benefits' => $this->benefit_model->fetchBenefits($id),
            'categories' => $this->basic_model->fetch_all_records(TBL_BENEFIT_CATEGORIES),
            
        ];

        $this->user_nav_lib->run_page('settings/benefits/list', $pageData, 'Benefits | ' . BUSINESS_NAME);
    }
    
    

    public function index() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->settings_model->savePlan(request_post_data())) {
                notify('success', "Health plan added successfully");
            } else {
                notify('error', "Unable to add health plan at moment, pls try again later");
            }

            redirect(site_url('settings/plans'));
        }

        $pageData = [
            'plans' => $this->basic_model->fetch_all_records(TBL_PLANS)
        ];

        $this->user_nav_lib->run_page('settings/plans/list', $pageData, 'Health Plans | ' . BUSINESS_NAME);
    }
    public function benefit_categories() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->settings_model->saveBenefitCategory(request_post_data())) {
                notify('success', "Benefit Category added successfully");
            } else {
                notify('error', "Unable to add benefit category at moment, pls try again later");
            }

            redirect(site_url('settings/benefit_categories'));
        }

        $pageData = [
            'benefit_categories' => $this->basic_model->fetch_all_records(TBL_BENEFIT_CATEGORIES)
        ];

        $this->user_nav_lib->run_page('settings/benefit_categories/list', $pageData, 'Benefit Category | ' . BUSINESS_NAME);
    }

    public function delete_plan($id) {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->check_perm('health_plan:delete_plan');
        if ($this->basic_model->delete(TBL_PLANS, ['health_plan_id' => $id])) {
            notify('success', 'Health plan deleted successfully');
        } else {
            notify('error', 'Oops! Unable to delete health plan');
        }
        redirect($this->input->server('HTTP_REFERER'));
    }
    public function delete_benefit($id) {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->check_perm('health_plan:delete_plan');
        if ($this->basic_model->delete(TBL_BENEFITS, ['benefit_id' => $id])) {
            notify('success', 'Benefit deleted successfully');
        } else {
            notify('error', 'Oops! Unable to delete benefit');
        }
        redirect($this->input->server('HTTP_REFERER'));
    }
    
    public function delete_benefit_category($id) {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->check_perm('health_plan:delete_plan');
        if ($this->basic_model->delete(TBL_BENEFIT_CATEGORIES, ['benefit_category_id' => $id])) {
            notify('success', 'Benefit Category deleted successfully');
        } else {
            notify('error', 'Oops! Unable to delete benefit category');
        }
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function remove_benefit($id) {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->check_perm('health_plan:delete_benefit');
        
        if ($this->basic_model->delete(TBL_PLAN_BENEFITS, ['health_plan_benefit_id' => $id])) {
            notify('success', 'Plan benefit removed successfully');
        } else {
            notify('error', 'Oops! Unable to remove plan benefit');
        }
        redirect($this->input->server('HTTP_REFERER'));
    }

    public function plan_benefits($plan_id) {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->check_perm('health_plan:plan_benefits');
        
        if (request_is_post()) {
            if ($this->settings_model->saveBenefit(request_post_data())) {
                notify('success', "Plan Benefit added successfully");
            } else {
                notify('error', "Unable to add plan benefit at moment, pls try again later");
            }

            redirect(site_url("/settings/plans/benefits/{$plan_id}"));
        }

        $pageData = [
            'plan' => $this->basic_model->fetch_all_records(TBL_PLANS, ['health_plan_id' => $plan_id])[0],
            'benefits' => $this->benefit_model->fetchPlanBenefits($plan_id),
            'n_benefits' => $this->basic_model->fetch_all_records(TBL_BENEFITS),
        ];

        $this->user_nav_lib->run_page('settings/plans/benefits', $pageData, 'Health Plans | ' . BUSINESS_NAME);
    }

    public function edit_plan($id) {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->check_perm('health_plan:edit_plan');

        if (request_is_post()) {
            if ($this->basic_model->update(TBL_PLANS, request_post_data(), ['health_plan_id' => $id])) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Unable to update health plan at moment, Please try again later");
            }
            redirect(site_url('/settings/plans'));
        }

        $data = [
            'plan' => $this->basic_model->fetch_all_records(TBL_PLANS, ['health_plan_id' => $id])[0]
        ];

        $this->load->view('settings/plans/edit_plan', $data);
    }
    public function edit_benefit_category($id) {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->check_perm('health_plan:edit_plan');

        if (request_is_post()) {
            if ($this->basic_model->update(TBL_BENEFIT_CATEGORIES, request_post_data(), ['benefit_category_id' => $id])) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Unable to update benefit category at moment, Please try again later");
            }
            redirect(site_url('/settings/benefit_categories'));
        }

        $data = [
            'b_category' => $this->basic_model->fetch_all_records(TBL_BENEFIT_CATEGORIES, ['benefit_category_id' => $id])[0]
        ];

        $this->load->view('settings/benefit_categories/edit_benefit_category', $data);
    }
    
    public function edit_benefit($id) {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->check_perm('health_plan:edit_benefit');
        
        if (request_is_post()) {
            if ($this->basic_model->update(TBL_PLAN_BENEFITS, request_post_data(), ['health_plan_benefit_id' => $id])) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Unable to update health plan at moment, Please try again later");
            }
            redirect($this->input->server('HTTP_REFERER'));
        }
        
        $data = [
            'benefit' => $this->basic_model->fetch_all_records(TBL_PLAN_BENEFITS, ['health_plan_benefit_id' => $id])[0]
        ];

        $this->load->view('settings/plans/edit_benefit', $data);
        
    }
    
    public function edit_all_benefit($id) {
        $this->user_auth_lib->check_login();
        $this->user_auth_lib->check_perm('health_plan:edit_benefit');
        
        if (request_is_post()) {
            if ($this->basic_model->update(TBL_BENEFITS, request_post_data(), ['benefit_id' => $id])) {
                notify('success', "Operation was successful");
            } else {
                notify('error', "Unable to update benefit at moment, Please try again later");
            }
            redirect($this->input->server('HTTP_REFERER'));
        }
        
        $data = [
            'e_benefit' => $this->basic_model->fetch_all_records(TBL_BENEFITS, ['benefit_id' => $id])[0],
            'categories' => $this->basic_model->fetch_all_records(TBL_BENEFIT_CATEGORIES),
        ];

        $this->load->view('settings/benefits/edit_benefit', $data);
        
    }

}
