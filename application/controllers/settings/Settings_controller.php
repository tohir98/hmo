<?php

/**
 * Description of Settings_controller
 *
 * @author tohir
 * @property User_auth_lib $user_auth_lib Description
 * @property Settings_model $settings_model Description
 * @property Basic_model $basic_model Description
 */
class Settings_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('user/User_model', 'u_model');
        $this->load->model('settings/settings_model');
        $this->load->model('basic_model');
        $this->load->model('settings_model');
        $this->load->library(['User_nav_lib', 'mailer']);

        $this->user_auth_lib->check_login();
        $this->settings_model->saveDefaultMessage();
    }

    public function messages() {
        $this->user_auth_lib->check_perm('setup:messages');
        $pageData = [
            'settings' => $this->basic_model->fetch_all_records(Settings_model::TBL_MGS_SETTINGS)[0]
        ];

        if (request_is_post()) {
            if ($this->basic_model->update(Settings_model::TBL_MGS_SETTINGS, request_post_data(), [])) {
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Ops! Unable to complete your request at moment, Please try again later');
            }
            redirect(site_url('/settings/messages'));
        }
        $this->user_nav_lib->run_page('settings/messages', $pageData, 'Message Settings | ' . BUSINESS_NAME);
    }

}
