ALTER TABLE `premium_payments` DROP `month`, DROP `year`;
ALTER TABLE  `premium_payments` ADD  `start_date` DATETIME NULL AFTER  `amount`;
ALTER TABLE  `premium_payments` ADD  `end_date` DATETIME NULL AFTER  `start_date`;