DROP TABLE IF EXISTS `hmo`.`benefit`;
CREATE TABLE  `hmo`.`benefit` (
  `benefit_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `benefit` varchar(150) NOT NULL,
  `benefit_desc` varchar(255) DEFAULT NULL,
  `capitated` tinyint(3) unsigned NOT NULL,
  `benefit_category_id` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`benefit_id`),
  KEY `FK_benefit_benefit_category_id` (`benefit_category_id`),
  CONSTRAINT `FK_benefit_benefit_category_id` FOREIGN KEY (`benefit_category_id`) REFERENCES `benefit_categories` (`benefit_category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
