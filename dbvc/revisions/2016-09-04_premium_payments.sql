CREATE TABLE IF NOT EXISTS `premium_payments` (
  `premium_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subscriber_id` int(11) unsigned DEFAULT NULL,
  `health_plan_id` int(11) unsigned DEFAULT NULL,
  `amount` int(11) DEFAULT '0',
  `month` varchar(255) DEFAULT NULL,
  `date_paid` datetime DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`premium_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;