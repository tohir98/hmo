
INSERT INTO `module_perms` (`perm_id`, `module_id`, `subject`, `id_string`, `in_menu`, `status`, `menu_order`) VALUES
(22, 2, 'Message Settings', 'messages', 0, 1, 100),
(23, 2, 'Edit Message', 'edit_message', 0, 1, 100),
(24, 6, 'View Health Plans', 'view', 0, 1, 100),
(25, 2, 'Add Hospital', 'add_hospital', 0, 1, 100),
(26, 2, 'Edit Hospital', 'edit_hospital', 0, 1, 100);


