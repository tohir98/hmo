DROP TABLE IF EXISTS `hmo`.`health_plan_benefits`;
CREATE TABLE  `hmo`.`health_plan_benefits` (
  `health_plan_benefit_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `benefit_id` int(10) unsigned NOT NULL,
  `health_plan_id` int(10) NOT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`health_plan_benefit_id`),
  KEY `FK_health_plan_benefits_benefit_id` (`benefit_id`),
  KEY `FK_health_plan_benefits_health_plan_id` (`health_plan_id`),
  CONSTRAINT `FK_health_plan_benefits_benefit_id` FOREIGN KEY (`benefit_id`) REFERENCES `benefit` (`benefit_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_health_plan_benefits_health_plan_id` FOREIGN KEY (`health_plan_id`) REFERENCES `health_plans` (`health_plan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
