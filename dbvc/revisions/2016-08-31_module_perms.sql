
INSERT INTO `modules` (`module_id`, `subject`, `id_string`, `status`, `menu_order`) VALUES
(1, 'Administration', 'admin', 1, 100),
(2, 'Setup', 'setup', 1, 100),
(3, 'Subscriber', 'subscriber', 1, 100),
(4, 'Access Control', 'access_control', 1, 100),
(5, 'Analytics', 'report', 1, 100);

INSERT INTO `module_perms` (`perm_id`, `module_id`, `subject`, `id_string`, `in_menu`, `menu_order`, `status`) VALUES
(null, 4, 'View Access Control', 'view_access_control', 1, 100, 1),
(null, 4, 'Edit Permission', 'edit_permission', 0, 100, 1),
(null, 3, 'Create Subscriber', 'create_subscriber', 1, 100, 1),
(null, 3, 'Edit Subscriber', 'edit_subscriber', 1, 100, 1),
(null, 3, 'Delete Subscriber', 'delete_subscriber', 1, 100, 1),
(null, 1, 'users', 'users', 1, 100, 1),
(null, 1, 'User Types', 'user_types', 1, 100, 1),
(null, 1, 'Change My Password', 'change_password', 1, 100, 1);