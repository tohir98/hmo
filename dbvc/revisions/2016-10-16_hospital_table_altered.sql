ALTER TABLE  `hospitals` ADD  `contact_name` VARCHAR (255) NOT NULL AFTER  `logo`;
ALTER TABLE  `hospitals` ADD  `contact_phone` VARCHAR (255) NOT NULL AFTER  `contact_name`;
ALTER TABLE  `hospitals` ADD  `contact_email` VARCHAR (255) NOT NULL AFTER  `contact_phone`;