CREATE TABLE IF NOT EXISTS `message_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sign_up_email` longtext NOT NULL,
  `sign_up_sms` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;