DROP TABLE IF EXISTS `hmo`.`benefit_categories`;
CREATE TABLE  `hmo`.`benefit_categories` (
  `benefit_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(150) NOT NULL,
  PRIMARY KEY (`benefit_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
