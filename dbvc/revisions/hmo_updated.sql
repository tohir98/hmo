/*
SQLyog Community v12.2.0 (64 bit)
MySQL - 5.7.9 : Database - hmo
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hmo` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hmo`;

/*Table structure for table `blood_groups` */

DROP TABLE IF EXISTS `blood_groups`;

CREATE TABLE `blood_groups` (
  `blood_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `blood_group` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`blood_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `enrollment_types` */

DROP TABLE IF EXISTS `enrollment_types`;

CREATE TABLE `enrollment_types` (
  `enrollment_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `enrollment_type` varchar(150) NOT NULL,
  `description` varchar(300) NOT NULL,
  PRIMARY KEY (`enrollment_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `enrollments` */

DROP TABLE IF EXISTS `enrollments`;

CREATE TABLE `enrollments` (
  `enrollment_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriber_type_id` int(11) NOT NULL,
  `health_plan_id` int(11) NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `enrollment_type_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`enrollment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `gender` */

DROP TABLE IF EXISTS `gender`;

CREATE TABLE `gender` (
  `gender_id` int(25) NOT NULL AUTO_INCREMENT,
  `gender` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `genotypes` */

DROP TABLE IF EXISTS `genotypes`;

CREATE TABLE `genotypes` (
  `genotype_id` int(11) NOT NULL AUTO_INCREMENT,
  `genotype` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`genotype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `health_plan_benefits` */

DROP TABLE IF EXISTS `health_plan_benefits`;

CREATE TABLE `health_plan_benefits` (
  `health_plan_benefit_id` int(11) NOT NULL AUTO_INCREMENT,
  `health_plan_id` int(11) NOT NULL,
  `plan_benefit` varchar(300) NOT NULL,
  `description` longtext,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`health_plan_benefit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `health_plans` */

DROP TABLE IF EXISTS `health_plans`;

CREATE TABLE `health_plans` (
  `health_plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `health_plan` varchar(150) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `premium` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`health_plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `hospital_phones` */

DROP TABLE IF EXISTS `hospital_phones`;

CREATE TABLE `hospital_phones` (
  `hospital_phone_id` int(11) NOT NULL AUTO_INCREMENT,
  `hospital_id` int(11) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  PRIMARY KEY (`hospital_phone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `hospitals` */

DROP TABLE IF EXISTS `hospitals`;

CREATE TABLE `hospitals` (
  `hospital_id` int(11) NOT NULL AUTO_INCREMENT,
  `hospital_name` varchar(150) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `address2` varchar(300) NOT NULL,
  `city` varchar(150) NOT NULL,
  `state_id` int(11) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`hospital_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `marital_status` */

DROP TABLE IF EXISTS `marital_status`;

CREATE TABLE `marital_status` (
  `marital_status_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `marital_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`marital_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Table structure for table `module_perms` */

DROP TABLE IF EXISTS `module_perms`;

CREATE TABLE `module_perms` (
  `perm_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `menu_order` smallint(6) DEFAULT '100',
  PRIMARY KEY (`perm_id`),
  KEY `FK_module_perms_module_id` (`module_id`),
  CONSTRAINT `module_perms_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `modules` */

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(150) DEFAULT NULL,
  `id_string` varchar(150) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `requires_login` int(10) unsigned DEFAULT NULL,
  `menu_order` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `state_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `state` varchar(100) NOT NULL,
  `id_country` int(255) unsigned NOT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

/*Table structure for table `subscriber_types` */

DROP TABLE IF EXISTS `subscriber_types`;

CREATE TABLE `subscriber_types` (
  `subscriber_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriber_type` varchar(150) NOT NULL,
  `description` varchar(300) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`subscriber_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `subscribers` */

DROP TABLE IF EXISTS `subscribers`;

CREATE TABLE `subscribers` (
  `subscriber_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `othernames` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender_id` int(11) unsigned DEFAULT NULL,
  `marital_status_id` int(11) unsigned DEFAULT NULL,
  `genotype_id` int(11) unsigned DEFAULT NULL,
  `blood_group_id` int(11) unsigned DEFAULT NULL,
  `profile_pic` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`subscriber_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `titles` */

DROP TABLE IF EXISTS `titles`;

CREATE TABLE `titles` (
  `title_id` int(25) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `user_logs` */

DROP TABLE IF EXISTS `user_logs`;

CREATE TABLE `user_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) unsigned NOT NULL,
  `message` text CHARACTER SET latin1 NOT NULL,
  `type` int(1) unsigned NOT NULL DEFAULT '1',
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_address` bigint(20) DEFAULT NULL,
  `user_agent` text,
  `session_id` text,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `url` text,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `token` text,
  `adminid` int(11) DEFAULT NULL,
  `user_visibility` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `user_perms` */

DROP TABLE IF EXISTS `user_perms`;

CREATE TABLE `user_perms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) unsigned NOT NULL,
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `user_type_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
