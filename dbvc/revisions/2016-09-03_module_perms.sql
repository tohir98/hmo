INSERT INTO `modules` (`module_id`, `subject`, `id_string`, `status`, `menu_order`) VALUES
(6, 'Health Plans', 'health_plan', 1, 100),
(7, 'Hospitals', 'hospitals', 1, 100);

INSERT INTO `module_perms` (`perm_id`, `module_id`, `subject`, `id_string`, `in_menu`, `menu_order`, `status`) VALUES
(null, 3, 'View Subscribers', 'view_subscriber', 1, 100, 1),
(null, 6, 'Add Plan', 'add_plan', 1, 100, 1),
(null, 6, 'Edit Health Plan', 'edit_plan', 1, 100, 1),
(null, 6, 'Delete Health Plan', 'delete_plan', 1, 100, 1),
(null, 6, 'Plan Benefits', 'plan_benefits', 1, 100, 1),
(null, 6, 'Add Benefits', 'add_benefit', 1, 100, 1),
(null, 6, 'Edit Benefits', 'edit_benefit', 1, 100, 1),
(null, 6, 'Delete Benefits', 'delete_benefits', 1, 100, 1),
(null, 7, 'Add Hospital', 'add_hospital', 1, 100, 1),
(null, 7, 'Edit Hospital', 'edit_hospital', 1, 100, 1),
(null, 7, 'Delete Hospital', 'delete_hospital', 1, 100, 1);


