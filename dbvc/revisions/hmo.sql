-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 16, 2016 at 11:53 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hmo`
--
CREATE DATABASE IF NOT EXISTS `hmo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hmo`;

-- --------------------------------------------------------

--
-- Table structure for table `blood_groups`
--

CREATE TABLE `blood_groups` (
  `blood_group_id` int(11) NOT NULL,
  `blood_group` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blood_groups`
--

INSERT INTO `blood_groups` (`blood_group_id`, `blood_group`, `description`) VALUES
(1, 'O+', 'O+'),
(2, 'O-', 'O-'),
(3, 'A+', 'A+'),
(4, 'A-', 'A-'),
(5, 'B+', 'B+'),
(6, 'B-', 'B-'),
(7, 'AB+', 'AB+');

-- --------------------------------------------------------

--
-- Table structure for table `enrollments`
--

CREATE TABLE `enrollments` (
  `enrollment_id` int(11) NOT NULL,
  `subscriber_type_id` int(11) NOT NULL,
  `health_plan_id` int(11) NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `title_id` int(11) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `surname` varchar(150) NOT NULL,
  `othernames` varchar(150) DEFAULT NULL,
  `gender_id` int(11) DEFAULT NULL,
  `marital_status_id` int(11) DEFAULT NULL,
  `enrollment_type_id` int(11) NOT NULL,
  `genotype_id` int(11) NOT NULL,
  `blood_group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `enrollment_types`
--

CREATE TABLE `enrollment_types` (
  `enrollment_type_id` int(11) NOT NULL,
  `enrollment_type` varchar(150) NOT NULL,
  `description` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enrollment_types`
--

INSERT INTO `enrollment_types` (`enrollment_type_id`, `enrollment_type`, `description`) VALUES
(1, 'Subscriber', 'Owner of plan'),
(2, 'dependant', 'Dependant');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `gender_id` int(25) NOT NULL,
  `gender` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`gender_id`, `gender`) VALUES
(1, 'Male'),
(2, 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `genotypes`
--

CREATE TABLE `genotypes` (
  `genotype_id` int(11) NOT NULL,
  `genotype` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genotypes`
--

INSERT INTO `genotypes` (`genotype_id`, `genotype`, `description`) VALUES
(1, 'AA', 'AA'),
(2, 'AS', 'AS'),
(3, 'SS', 'SS'),
(4, 'SC', 'SC'),
(5, 'Others', 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `health_plans`
--

CREATE TABLE `health_plans` (
  `health_plan_id` int(11) NOT NULL,
  `health_plan` varchar(150) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `premium` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `health_plan_benefits`
--

CREATE TABLE `health_plan_benefits` (
  `health_plan_benefit_id` int(11) NOT NULL,
  `health_plan_id` int(11) NOT NULL,
  `plan_benefit` varchar(300) NOT NULL,
  `description` longtext,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hospitals`
--

CREATE TABLE `hospitals` (
  `hospital_id` int(11) NOT NULL,
  `hospital_name` varchar(150) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `address2` varchar(300) NOT NULL,
  `city` varchar(150) NOT NULL,
  `state_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hospital_phones`
--

CREATE TABLE `hospital_phones` (
  `hospital_phone_id` int(11) NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `phone_number` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `marital_status`
--

CREATE TABLE `marital_status` (
  `marital_status_id` int(25) unsigned NOT NULL,
  `marital_status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `marital_status`
--

INSERT INTO `marital_status` (`marital_status_id`, `marital_status`) VALUES
(1, 'Single'),
(2, 'Married'),
(3, 'Separated'),
(4, 'Engaged');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(10) unsigned NOT NULL,
  `subject` varchar(150) DEFAULT NULL,
  `id_string` varchar(150) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `requires_login` int(10) unsigned DEFAULT NULL,
  `menu_order` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `module_perms`
--

CREATE TABLE `module_perms` (
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `menu_order` smallint(6) DEFAULT '100'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(255) unsigned NOT NULL,
  `state` varchar(100) NOT NULL,
  `id_country` int(255) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state`, `id_country`) VALUES
(1, 'Abia', 154),
(2, 'Abuja', 154),
(3, 'Adamawa', 154),
(4, 'Akwa Ibom', 154),
(5, 'Anambara', 154),
(6, 'Bauchi', 154),
(7, 'Bayelsa', 154),
(8, 'Benue', 154),
(9, 'Borno', 154),
(10, 'Cross River', 154),
(11, 'Delta', 154),
(12, 'Ebonyi', 154),
(13, 'Edo', 154),
(14, 'Ekiti', 154),
(15, 'Enugu', 154),
(16, 'Gombe', 154),
(17, 'Imo', 154),
(18, 'Jigawa', 154),
(19, 'Kaduna', 154),
(20, 'Kano', 154),
(21, 'Katsina', 154),
(22, 'Kebbi', 154),
(23, 'Kogi', 154),
(24, 'Kwara', 154),
(25, 'Lagos', 154),
(26, 'Nassarawa', 154),
(27, 'Niger', 154),
(28, 'Ogun', 154),
(29, 'Ondo', 154),
(30, 'Osun', 154),
(31, 'Oyo', 154),
(32, 'Plateau', 154),
(33, 'Rivers', 154),
(34, 'Sokoto', 154),
(35, 'Taraba', 154),
(36, 'Yobe', 154),
(37, 'Zamfara', 154);

-- --------------------------------------------------------

--
-- Table structure for table `subscriber_types`
--

CREATE TABLE `subscriber_types` (
  `subscriber_type_id` int(11) NOT NULL,
  `subscriber_type` varchar(150) NOT NULL,
  `description` varchar(300) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `title_id` int(25) NOT NULL,
  `title` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`title_id`, `title`) VALUES
(1, 'Mr.'),
(2, 'Mrs.'),
(3, 'Miss.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `user_type_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`, `status`, `user_type_id`, `date_created`, `date_updated`) VALUES
(1, 'Raheemah', 'Muritala', 'mmuritalaraimot@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 2, NULL, '2015-12-27 05:47:19'),
(2, 'sade', 'samulr', 'sa@aol.com', 'f338b5bbfb9029d8dd24901cc53a84872feefcfc', 1, 1, '2015-12-04 11:07:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE `user_logs` (
  `id` bigint(20) NOT NULL,
  `user_id` int(255) unsigned NOT NULL,
  `message` text CHARACTER SET latin1 NOT NULL,
  `type` int(1) unsigned NOT NULL DEFAULT '1',
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_address` bigint(20) DEFAULT NULL,
  `user_agent` text,
  `session_id` text,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `url` text,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `token` text,
  `adminid` int(11) DEFAULT NULL,
  `user_visibility` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_perms`
--

CREATE TABLE `user_perms` (
  `id` int(11) NOT NULL,
  `user_id` int(255) unsigned NOT NULL,
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blood_groups`
--
ALTER TABLE `blood_groups`
  ADD PRIMARY KEY (`blood_group_id`);

--
-- Indexes for table `enrollments`
--
ALTER TABLE `enrollments`
  ADD PRIMARY KEY (`enrollment_id`);

--
-- Indexes for table `enrollment_types`
--
ALTER TABLE `enrollment_types`
  ADD PRIMARY KEY (`enrollment_type_id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`gender_id`);

--
-- Indexes for table `genotypes`
--
ALTER TABLE `genotypes`
  ADD PRIMARY KEY (`genotype_id`);

--
-- Indexes for table `health_plans`
--
ALTER TABLE `health_plans`
  ADD PRIMARY KEY (`health_plan_id`);

--
-- Indexes for table `health_plan_benefits`
--
ALTER TABLE `health_plan_benefits`
  ADD PRIMARY KEY (`health_plan_benefit_id`);

--
-- Indexes for table `hospitals`
--
ALTER TABLE `hospitals`
  ADD PRIMARY KEY (`hospital_id`);

--
-- Indexes for table `hospital_phones`
--
ALTER TABLE `hospital_phones`
  ADD PRIMARY KEY (`hospital_phone_id`);

--
-- Indexes for table `marital_status`
--
ALTER TABLE `marital_status`
  ADD PRIMARY KEY (`marital_status_id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `module_perms`
--
ALTER TABLE `module_perms`
  ADD PRIMARY KEY (`perm_id`),
  ADD KEY `FK_module_perms_module_id` (`module_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `subscriber_types`
--
ALTER TABLE `subscriber_types`
  ADD PRIMARY KEY (`subscriber_type_id`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`title_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_perms`
--
ALTER TABLE `user_perms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blood_groups`
--
ALTER TABLE `blood_groups`
  MODIFY `blood_group_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `enrollments`
--
ALTER TABLE `enrollments`
  MODIFY `enrollment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `enrollment_types`
--
ALTER TABLE `enrollment_types`
  MODIFY `enrollment_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `gender_id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `genotypes`
--
ALTER TABLE `genotypes`
  MODIFY `genotype_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `health_plans`
--
ALTER TABLE `health_plans`
  MODIFY `health_plan_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `health_plan_benefits`
--
ALTER TABLE `health_plan_benefits`
  MODIFY `health_plan_benefit_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hospitals`
--
ALTER TABLE `hospitals`
  MODIFY `hospital_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hospital_phones`
--
ALTER TABLE `hospital_phones`
  MODIFY `hospital_phone_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `marital_status`
--
ALTER TABLE `marital_status`
  MODIFY `marital_status_id` int(25) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `module_perms`
--
ALTER TABLE `module_perms`
  MODIFY `perm_id` int(255) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(255) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `subscriber_types`
--
ALTER TABLE `subscriber_types`
  MODIFY `subscriber_type_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `title_id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_perms`
--
ALTER TABLE `user_perms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `module_perms`
--
ALTER TABLE `module_perms`
  ADD CONSTRAINT `module_perms_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
