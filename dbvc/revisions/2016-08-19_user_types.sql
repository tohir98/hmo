CREATE TABLE IF NOT EXISTS `user_types` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(150) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `user_types` (`user_type_id`, `user_type`, `status`) VALUES
(1, 'Admin', 1),
(2, 'User', 1),
(3, 'Subscribers', 1);
