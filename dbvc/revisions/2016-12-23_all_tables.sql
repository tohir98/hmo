-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 23, 2016 at 09:40 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `hmo`
--

-- --------------------------------------------------------

--
-- Table structure for table `blood_groups`
--

CREATE TABLE `blood_groups` (
  `blood_group_id` int(11) NOT NULL,
  `blood_group` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blood_groups`
--

INSERT INTO `blood_groups` (`blood_group_id`, `blood_group`, `description`) VALUES
(1, 'O+', 'O+'),
(2, 'O-', 'O-'),
(3, 'A+', 'A+'),
(4, 'A-', 'A-'),
(5, 'B+', 'B+'),
(6, 'B-', 'B-'),
(7, 'AB+', 'AB+');

-- --------------------------------------------------------

--
-- Table structure for table `enrollments`
--

CREATE TABLE `enrollments` (
  `enrollment_id` int(11) NOT NULL,
  `subscriber_type_id` int(11) NOT NULL,
  `health_plan_id` int(11) NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `enrollment_type_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enrollments`
--

INSERT INTO `enrollments` (`enrollment_id`, `subscriber_type_id`, `health_plan_id`, `hospital_id`, `subscriber_id`, `enrollment_type_id`, `created_by`, `created_at`, `is_deleted`) VALUES
(1, 0, 2, 2, 1, 1, 2, '2016-08-29 01:41:05', 0),
(2, 0, 2, 2, 4, 1, 2, '2016-10-01 07:36:52', 1),
(3, 0, 2, 2, 4, 1, 2, '2016-10-13 02:20:28', 0);

-- --------------------------------------------------------

--
-- Table structure for table `enrollment_types`
--

CREATE TABLE `enrollment_types` (
  `enrollment_type_id` int(11) NOT NULL,
  `enrollment_type` varchar(150) NOT NULL,
  `description` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enrollment_types`
--

INSERT INTO `enrollment_types` (`enrollment_type_id`, `enrollment_type`, `description`) VALUES
(1, 'Subscriber', 'Owner of plan'),
(2, 'dependant', 'Dependant');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `gender_id` int(25) NOT NULL,
  `gender` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`gender_id`, `gender`) VALUES
(1, 'Male'),
(2, 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `genotypes`
--

CREATE TABLE `genotypes` (
  `genotype_id` int(11) NOT NULL,
  `genotype` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genotypes`
--

INSERT INTO `genotypes` (`genotype_id`, `genotype`, `description`) VALUES
(1, 'AA', 'AA'),
(2, 'AS', 'AS'),
(3, 'SS', 'SS'),
(4, 'SC', 'SC'),
(5, 'Others', 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `health_plans`
--

CREATE TABLE `health_plans` (
  `health_plan_id` int(11) NOT NULL,
  `health_plan` varchar(150) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `premium` varchar(150) NOT NULL,
  `max_dependant` int(11) NOT NULL DEFAULT '6',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `health_plans`
--

INSERT INTO `health_plans` (`health_plan_id`, `health_plan`, `description`, `premium`, `max_dependant`, `created_by`, `created_at`) VALUES
(2, 'Gold', 'Golden plan', '45000', 6, 2, '2016-08-23 15:31:43'),
(3, 'Silver', 'Silver plan', '1500', 6, 2, '2016-08-23 15:32:01'),
(4, 'Platinum', 'Platinum plan', '10000', 5, 2, '2016-08-23 23:19:47');

-- --------------------------------------------------------

--
-- Table structure for table `health_plan_benefits`
--

CREATE TABLE `health_plan_benefits` (
  `health_plan_benefit_id` int(11) NOT NULL,
  `health_plan_id` int(11) NOT NULL,
  `plan_benefit` varchar(300) NOT NULL,
  `description` longtext,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `health_plan_benefits`
--

INSERT INTO `health_plan_benefits` (`health_plan_benefit_id`, `health_plan_id`, `plan_benefit`, `description`, `created_by`, `created_at`, `updated_at`, `updated_by`) VALUES
(1, 2, 'Fantastic', 'You gonna enjoy it very well', 2, '2016-08-23 16:05:11', NULL, NULL),
(2, 2, 'Money Back', 'Your money is secure very well.', 2, '2016-08-23 16:06:06', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hospitals`
--

CREATE TABLE `hospitals` (
  `hospital_id` int(11) NOT NULL,
  `capitation_id` int(11) DEFAULT '0',
  `hospital_name` varchar(150) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `address2` varchar(300) NOT NULL,
  `city` varchar(150) NOT NULL,
  `state_id` int(11) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_phone` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospitals`
--

INSERT INTO `hospitals` (`hospital_id`, `capitation_id`, `hospital_name`, `address1`, `address2`, `city`, `state_id`, `phone`, `logo`, `contact_name`, `contact_phone`, `contact_email`, `created_by`, `created_at`) VALUES
(1, 1, 'Motolano', 'Alfa nla street, agege', '', 'Agege', 25, '08053449988', 'f60ccbd75b94a5118a4d2f646643dd67.jpg', 'Liadi Aron', '08099447744', 'liadi@aa.com', 2, '2016-08-23 12:07:18'),
(2, 2, 'Nicholas Hospital', 'lagos Island', '', 'Lagos', 3, '0995556645', 'b7eb681c06c3da0c0dbf0bdcc3604d13.jpg', 'Emeka Salawu', '08123456754', 'emeka@sala.net', 2, '2016-08-29 01:40:26'),
(3, 0, 'Randle', 'Akerele', '', 'Surulere', 25, NULL, 'default_logo.png', '', '', '', 2, '2016-08-29 05:05:38'),
(4, 0, 'Saki Hospital', 'Saki street', '', 'Saki', 31, '98546574', 'default_logo.png', '', '', '', 2, '2016-08-29 06:08:21');

-- --------------------------------------------------------

--
-- Table structure for table `hospital_class`
--

CREATE TABLE `hospital_class` (
  `id` int(11) NOT NULL,
  `class` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `capitation` varchar(255) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospital_class`
--

INSERT INTO `hospital_class` (`id`, `class`, `description`, `capitation`, `created_by`, `date_created`) VALUES
(1, 'Class A', 'Class A', '2500', 2, '2016-10-21 04:56:30'),
(2, 'Class B', 'Class B', '3000', 2, '2016-10-21 04:56:56');

-- --------------------------------------------------------

--
-- Table structure for table `hospital_phones`
--

CREATE TABLE `hospital_phones` (
  `hospital_phone_id` int(11) NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `phone_number` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `marital_status`
--

CREATE TABLE `marital_status` (
  `marital_status_id` int(25) unsigned NOT NULL,
  `marital_status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `marital_status`
--

INSERT INTO `marital_status` (`marital_status_id`, `marital_status`) VALUES
(1, 'Single'),
(2, 'Married'),
(3, 'Separated'),
(4, 'Engaged');

-- --------------------------------------------------------

--
-- Table structure for table `message_settings`
--

CREATE TABLE `message_settings` (
  `id` int(11) NOT NULL,
  `sign_up_email` longtext NOT NULL,
  `sign_up_sms` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_settings`
--

INSERT INTO `message_settings` (`id`, `sign_up_email`, `sign_up_sms`, `date_added`, `added_by`, `date_updated`, `updated_by`) VALUES
(1, 'mes', 'Welcome to healthcare International.', '2016-10-08 15:21:27', 2, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(10) unsigned NOT NULL,
  `subject` varchar(150) DEFAULT NULL,
  `id_string` varchar(150) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `requires_login` int(10) unsigned DEFAULT NULL,
  `menu_order` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `subject`, `id_string`, `status`, `requires_login`, `menu_order`) VALUES
(1, 'Administration', 'admin', 1, NULL, 100),
(2, 'Setup', 'setup', 1, NULL, 100),
(3, 'Subscriber', 'subscriber', 1, NULL, 100),
(4, 'Access Control', 'access_control', 1, NULL, 100),
(5, 'Analytics', 'report', 1, NULL, 100),
(6, 'Health Plans', 'health_plan', 1, NULL, 100);

-- --------------------------------------------------------

--
-- Table structure for table `module_perms`
--

CREATE TABLE `module_perms` (
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `menu_order` smallint(6) DEFAULT '100'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module_perms`
--

INSERT INTO `module_perms` (`perm_id`, `module_id`, `subject`, `id_string`, `in_menu`, `status`, `menu_order`) VALUES
(3, 4, 'View Access Control', 'view_access_control', 1, 1, 100),
(4, 4, 'Edit Permission', 'edit_permission', 0, 1, 100),
(5, 1, 'users', 'users', 1, 1, 100),
(6, 1, 'User Types', 'user_types', 1, 1, 100),
(7, 1, 'Change My Password', 'change_password', 1, 1, 100),
(8, 3, 'Create Subscriber', 'create_subscriber', 1, 1, 100),
(9, 3, 'Edit Subscriber', 'edit_subscriber', 1, 1, 100),
(10, 3, 'Delete Subscriber', 'delete_subscriber', 1, 1, 100),
(11, 3, 'View Subscribers', 'view_subscriber', 1, 1, 100),
(15, 6, 'Add Plan', 'add_plan', 1, 1, 100),
(16, 6, 'Edit Health Plan', 'edit_plan', 1, 1, 100),
(17, 6, 'Delete Health Plan', 'delete_plan', 1, 1, 100),
(18, 6, 'Plan Benefits', 'plan_benefits', 1, 1, 100),
(19, 6, 'Add Benefits', 'add_benefit', 1, 1, 100),
(20, 6, 'Edit Benefits', 'edit_benefit', 1, 1, 100),
(21, 6, 'Delete Benefits', 'delete_benefits', 1, 1, 100),
(22, 2, 'Message Settings', 'messages', 0, 1, 100),
(23, 2, 'Edit Message', 'edit_message', 0, 1, 100),
(24, 6, 'View Health Plans', 'view', 0, 1, 100),
(25, 2, 'Add Hospital', 'add_hospital', 0, 1, 100),
(26, 2, 'Edit Hospital', 'edit_hospital', 0, 1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `premium_payments`
--

CREATE TABLE `premium_payments` (
  `premium_id` int(11) unsigned NOT NULL,
  `subscriber_id` int(11) unsigned DEFAULT NULL,
  `health_plan_id` int(11) unsigned DEFAULT NULL,
  `amount` int(11) DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `date_paid` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `premium_payments`
--

INSERT INTO `premium_payments` (`premium_id`, `subscriber_id`, `health_plan_id`, `amount`, `start_date`, `end_date`, `date_paid`, `created_at`, `is_deleted`) VALUES
(1, 1, 2, 45000, NULL, NULL, '2016-09-05 08:01:12', '2016-09-05 08:01:12', 1),
(2, 1, 2, 45000, NULL, NULL, '2016-09-05 08:06:26', '2016-09-05 08:06:26', 1),
(3, 1, 2, 45000, NULL, NULL, '2016-09-05 08:06:38', '2016-09-05 08:06:38', 1),
(4, 1, 2, 45000, '2016-07-01 12:00:00', '2016-10-31 12:00:00', '2016-10-21 05:26:07', '2016-10-21 05:26:07', 1),
(5, 1, 2, 45000, '2016-07-01 12:00:00', '2016-10-31 12:00:00', '2016-10-21 08:39:25', '2016-10-21 08:39:25', 1),
(6, 1, 2, 45000, '2016-07-01 12:00:00', '2016-10-31 12:00:00', '2016-10-21 08:40:23', '2016-10-21 08:40:23', 1),
(7, 1, 2, 45000, '2016-07-01 12:00:00', '2016-10-31 12:00:00', '2016-10-21 08:41:14', '2016-10-21 08:41:14', 1),
(8, 1, 2, 45000, '2016-07-01 12:00:00', '2016-10-31 12:00:00', '2016-10-21 08:42:25', '2016-10-21 08:42:25', 1),
(9, 1, 2, 45000, '2016-05-01 12:00:00', '2016-12-31 12:00:00', '2016-10-22 04:12:24', '2016-10-22 04:12:24', 0),
(10, 1, 2, 15000, '2017-01-01 12:00:00', '2017-03-31 12:00:00', '2016-10-22 04:16:51', '2016-10-22 04:16:51', 0);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(255) unsigned NOT NULL,
  `state` varchar(100) NOT NULL,
  `id_country` int(255) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state`, `id_country`) VALUES
(1, 'Abia', 154),
(2, 'Abuja', 154),
(3, 'Adamawa', 154),
(4, 'Akwa Ibom', 154),
(5, 'Anambara', 154),
(6, 'Bauchi', 154),
(7, 'Bayelsa', 154),
(8, 'Benue', 154),
(9, 'Borno', 154),
(10, 'Cross River', 154),
(11, 'Delta', 154),
(12, 'Ebonyi', 154),
(13, 'Edo', 154),
(14, 'Ekiti', 154),
(15, 'Enugu', 154),
(16, 'Gombe', 154),
(17, 'Imo', 154),
(18, 'Jigawa', 154),
(19, 'Kaduna', 154),
(20, 'Kano', 154),
(21, 'Katsina', 154),
(22, 'Kebbi', 154),
(23, 'Kogi', 154),
(24, 'Kwara', 154),
(25, 'Lagos', 154),
(26, 'Nassarawa', 154),
(27, 'Niger', 154),
(28, 'Ogun', 154),
(29, 'Ondo', 154),
(30, 'Osun', 154),
(31, 'Oyo', 154),
(32, 'Plateau', 154),
(33, 'Rivers', 154),
(34, 'Sokoto', 154),
(35, 'Taraba', 154),
(36, 'Yobe', 154),
(37, 'Zamfara', 154);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `subscriber_id` int(11) unsigned NOT NULL,
  `subscriber_type_id` int(11) NOT NULL,
  `subscriber_parent_id` int(11) NOT NULL DEFAULT '0',
  `title_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `othernames` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender_id` int(11) unsigned DEFAULT NULL,
  `marital_status_id` int(11) unsigned DEFAULT NULL,
  `genotype_id` int(11) unsigned DEFAULT NULL,
  `blood_group_id` int(11) unsigned DEFAULT NULL,
  `profile_pic` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`subscriber_id`, `subscriber_type_id`, `subscriber_parent_id`, `title_id`, `first_name`, `othernames`, `surname`, `address`, `phone`, `email`, `gender_id`, `marital_status_id`, `genotype_id`, `blood_group_id`, `profile_pic`, `created_by`, `created_at`) VALUES
(1, 1, 0, 1, 'Tohir', 'Oyedele', 'Omoloye', '10 Bola Shadipe Street, Off Adelabu, Surulere', '08037816587', 'info@aol.com', 2, 2, 1, 1, NULL, 2, '2016-08-29 01:19:03'),
(2, 2, 1, 1, 'taiye', '', 'taiwo', '11 Ojularede street', '8099887766', '', 2, 2, 1, 2, NULL, 2, '2016-09-05 07:40:06'),
(3, 2, 1, 2, 'taiye', '', 'taiwo', '11 Ojularede street', '8037816587', '', 1, 2, 3, 1, NULL, 2, '2016-09-05 07:49:05'),
(4, 1, 0, 3, 'Umuani', 'Titi', 'Liadi', '10 Bola Shadipe Street, Off Adelabu, Surulere', '08148771785', 'otcleantech54@gmail.com', 2, 1, 2, 2, '88996b6134706c85ca5df0621d76fce5.jpg', 2, '2016-10-01 07:28:30'),
(5, 1, 0, 2, 'Nafiu', '', 'John', '11 Ebolo street, Sabo, Yaba', '08052400465', 'info@mnet.org', 2, 2, 2, 2, 'd89405dd968660b34a444ce21054b422.jpg', 2, '2016-10-13 01:52:34'),
(6, 1, 0, 2, 'Nafiu', '', 'John', '11 Ebolo street, Sabo, Yaba', '08052400465', 'info@mnet.org', 2, 2, 2, 2, '1f62357b7e9e5fbc8bb256ffdfab7126.jpg', 2, '2016-10-13 01:55:56'),
(7, 1, 0, 2, 'Nafiu', '', 'John', '11 Ebolo street, Sabo, Yaba', '08052400465', 'info@mnet.org', 2, 2, 2, 2, 'efc2ae1ffcec2a1fe15cbd255ae367fb.jpg', 2, '2016-10-13 01:56:09'),
(8, 1, 0, 2, 'Nafiu', '', 'John', '11 Ebolo street, Sabo, Yaba', '08052400465', 'info@mnet.org', 2, 2, 2, 2, '0fadd11488f4034fbe844a94967ff89a.jpg', 2, '2016-10-13 01:57:10'),
(9, 1, 0, 2, 'Nafiu', '', 'John', '11 Ebolo street, Sabo, Yaba', '08052400465', 'info@mnet.org', 2, 2, 2, 2, '5f7840085b104e623a21536e21b893b4.jpg', 2, '2016-10-13 02:00:44'),
(10, 1, 0, 1, 'na', '', 'na', 'na', '8037116500', 'mktmdghsdgr9@aol.com', 2, 2, 2, 3, NULL, 2, '2016-10-13 02:01:27'),
(11, 1, 0, 1, 'na', '', 'na', 'na', '8037116500', 'mktmgr9@aol.com', 2, 2, 2, 3, NULL, 2, '2016-10-13 02:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `subscriber_types`
--

CREATE TABLE `subscriber_types` (
  `subscriber_type_id` int(11) NOT NULL,
  `subscriber_type` varchar(150) NOT NULL,
  `description` varchar(300) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `title_id` int(25) NOT NULL,
  `title` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`title_id`, `title`) VALUES
(1, 'Mr.'),
(2, 'Mrs.'),
(3, 'Miss.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `user_type_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`, `status`, `user_type_id`, `date_created`, `date_updated`) VALUES
(1, 'Raheemah', 'Muritala', 'mmuritalaraimot@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 2, NULL, '2015-12-27 05:47:19'),
(2, 'sade', 'samulr', 'sa@aol.com', 'f338b5bbfb9029d8dd24901cc53a84872feefcfc', 1, 1, '2015-12-04 11:07:53', NULL),
(3, 'TDHG', 'JDSH', 'sasa@sasa.sa', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 1, 1, '2016-08-19 07:10:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE `user_logs` (
  `id` bigint(20) NOT NULL,
  `user_id` int(255) unsigned NOT NULL,
  `message` text CHARACTER SET latin1 NOT NULL,
  `type` int(1) unsigned NOT NULL DEFAULT '1',
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_address` bigint(20) DEFAULT NULL,
  `user_agent` text,
  `session_id` text,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `url` text,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `token` text,
  `adminid` int(11) DEFAULT NULL,
  `user_visibility` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_perms`
--

CREATE TABLE `user_perms` (
  `id` int(11) NOT NULL,
  `user_id` int(255) unsigned NOT NULL,
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_perms`
--

INSERT INTO `user_perms` (`id`, `user_id`, `perm_id`, `module_id`) VALUES
(1, 1, 3, 1),
(2, 1, 10, 2),
(3, 1, 9, 2),
(4, 1, 4, 2),
(5, 1, 5, 2),
(6, 1, 6, 2),
(7, 1, 7, 2),
(8, 1, 8, 2),
(10, 1, 11, 3),
(11, 2, 3, 4),
(12, 2, 4, 4),
(13, 2, 5, 1),
(14, 2, 6, 1),
(15, 2, 7, 1),
(16, 2, 8, 3),
(17, 2, 9, 3),
(18, 2, 10, 3),
(19, 3, 7, 1),
(20, 3, 6, 1),
(22, 2, 11, 3),
(23, 3, 11, 3),
(24, 3, 9, 3),
(25, 2, 15, 6),
(26, 2, 16, 6),
(27, 2, 17, 6),
(28, 2, 18, 6),
(29, 2, 19, 6),
(30, 2, 20, 6),
(31, 2, 21, 6),
(32, 2, 22, 2),
(33, 2, 23, 2),
(35, 2, 24, 6),
(36, 2, 25, 2),
(37, 2, 26, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `user_type_id` int(11) NOT NULL,
  `user_type` varchar(150) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`user_type_id`, `user_type`, `status`) VALUES
(1, 'Admin', 1),
(2, 'User', 1),
(3, 'Subscribers', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blood_groups`
--
ALTER TABLE `blood_groups`
  ADD PRIMARY KEY (`blood_group_id`);

--
-- Indexes for table `enrollments`
--
ALTER TABLE `enrollments`
  ADD PRIMARY KEY (`enrollment_id`);

--
-- Indexes for table `enrollment_types`
--
ALTER TABLE `enrollment_types`
  ADD PRIMARY KEY (`enrollment_type_id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`gender_id`);

--
-- Indexes for table `genotypes`
--
ALTER TABLE `genotypes`
  ADD PRIMARY KEY (`genotype_id`);

--
-- Indexes for table `health_plans`
--
ALTER TABLE `health_plans`
  ADD PRIMARY KEY (`health_plan_id`);

--
-- Indexes for table `health_plan_benefits`
--
ALTER TABLE `health_plan_benefits`
  ADD PRIMARY KEY (`health_plan_benefit_id`);

--
-- Indexes for table `hospitals`
--
ALTER TABLE `hospitals`
  ADD PRIMARY KEY (`hospital_id`);

--
-- Indexes for table `hospital_class`
--
ALTER TABLE `hospital_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hospital_phones`
--
ALTER TABLE `hospital_phones`
  ADD PRIMARY KEY (`hospital_phone_id`);

--
-- Indexes for table `marital_status`
--
ALTER TABLE `marital_status`
  ADD PRIMARY KEY (`marital_status_id`);

--
-- Indexes for table `message_settings`
--
ALTER TABLE `message_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `module_perms`
--
ALTER TABLE `module_perms`
  ADD PRIMARY KEY (`perm_id`),
  ADD KEY `FK_module_perms_module_id` (`module_id`);

--
-- Indexes for table `premium_payments`
--
ALTER TABLE `premium_payments`
  ADD PRIMARY KEY (`premium_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`subscriber_id`);

--
-- Indexes for table `subscriber_types`
--
ALTER TABLE `subscriber_types`
  ADD PRIMARY KEY (`subscriber_type_id`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`title_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_perms`
--
ALTER TABLE `user_perms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`user_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blood_groups`
--
ALTER TABLE `blood_groups`
  MODIFY `blood_group_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `enrollments`
--
ALTER TABLE `enrollments`
  MODIFY `enrollment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `enrollment_types`
--
ALTER TABLE `enrollment_types`
  MODIFY `enrollment_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `gender_id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `genotypes`
--
ALTER TABLE `genotypes`
  MODIFY `genotype_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `health_plans`
--
ALTER TABLE `health_plans`
  MODIFY `health_plan_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `health_plan_benefits`
--
ALTER TABLE `health_plan_benefits`
  MODIFY `health_plan_benefit_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hospitals`
--
ALTER TABLE `hospitals`
  MODIFY `hospital_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hospital_class`
--
ALTER TABLE `hospital_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hospital_phones`
--
ALTER TABLE `hospital_phones`
  MODIFY `hospital_phone_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `marital_status`
--
ALTER TABLE `marital_status`
  MODIFY `marital_status_id` int(25) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `message_settings`
--
ALTER TABLE `message_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `module_perms`
--
ALTER TABLE `module_perms`
  MODIFY `perm_id` int(255) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `premium_payments`
--
ALTER TABLE `premium_payments`
  MODIFY `premium_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(255) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `subscriber_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `subscriber_types`
--
ALTER TABLE `subscriber_types`
  MODIFY `subscriber_type_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `title_id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_perms`
--
ALTER TABLE `user_perms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `module_perms`
--
ALTER TABLE `module_perms`
  ADD CONSTRAINT `module_perms_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE;
