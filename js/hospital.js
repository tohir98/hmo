$('body').delegate('.edit_hospital', 'click', function (evt) {
    evt.preventDefault();

    $('#modal_edit_hospital').modal('show');
    $('#modal_edit_hospital').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

    var page = $(this).attr("href");
    $.get(page, function (html) {

        $('#modal_edit_hospital').html('');
        $('#modal_edit_hospital').html(html);
        $('#modal_edit_hospital').modal('show').fadeIn();
    });
    return false;
});

$(function () {
    $('body').delegate('.download_capitation', 'click', function (e) {
        e.preventDefault();
        var h = this.href;
        var message = "Are you sure you want to download this hospital's capitation list?";
        CTM.doConfirm({
            title: 'Download Capitation List',
            message: message,
            acceptText: 'Download',
            onAccept: function () {
                window.location = h;
            }
        });
    });
});